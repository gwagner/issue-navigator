/* in-personal-resource.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-personal-resource.h"

typedef struct {
  gchar *id;
  gchar *author_id;
} InPersonalResourcePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (InPersonalResource, in_personal_resource, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  PROP_AUTHOR_ID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * in_personal_resource_new:
 *
 * Create a new #InPersonalResource.
 *
 * Returns: (transfer full): a newly created #InPersonalResource
 */
InPersonalResource *
in_personal_resource_new (void)
{
  return g_object_new (IN_TYPE_PERSONAL_RESOURCE, NULL);
}

static void
in_personal_resource_finalize (GObject *object)
{
  InPersonalResource *self = (InPersonalResource *)object;
  InPersonalResourcePrivate *priv = in_personal_resource_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->author_id, g_free);

  G_OBJECT_CLASS (in_personal_resource_parent_class)->finalize (object);
}

static void
in_personal_resource_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  InPersonalResource *self = IN_PERSONAL_RESOURCE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, in_personal_resource_get_id (self));
      break;
    case PROP_AUTHOR_ID:
      g_value_set_string (value, in_personal_resource_get_author_id (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_personal_resource_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  InPersonalResource *self = IN_PERSONAL_RESOURCE (object);

  switch (prop_id)
    {
    case PROP_ID:
      in_personal_resource_set_id (self, g_value_get_string (value));
      break;
    case PROP_AUTHOR_ID:
      in_personal_resource_set_author_id (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_personal_resource_class_init (InPersonalResourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_personal_resource_finalize;
  object_class->get_property = in_personal_resource_get_property;
  object_class->set_property = in_personal_resource_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "Id",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_AUTHOR_ID] =
    g_param_spec_string ("author-id",
                         "AuthorId",
                         "AuthorId",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
in_personal_resource_init (InPersonalResource *self)
{
}

const gchar *
in_personal_resource_get_author_id (InPersonalResource *self)
{
  InPersonalResourcePrivate *priv = in_personal_resource_get_instance_private (self);

  g_return_val_if_fail (IN_IS_PERSONAL_RESOURCE (self), 0);

  return priv->author_id;
}

void
in_personal_resource_set_author_id (InPersonalResource *self,
                                    const gchar        *author_id)
{
  InPersonalResourcePrivate *priv = in_personal_resource_get_instance_private (self);

  g_return_if_fail (IN_IS_PERSONAL_RESOURCE (self));

  if (g_strcmp0 (priv->author_id, author_id) != 0)
    {
      priv->author_id = g_strdup (author_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_AUTHOR_ID]);
    }
}

const gchar *
in_personal_resource_get_id (InPersonalResource *self)
{
  InPersonalResourcePrivate *priv = in_personal_resource_get_instance_private (self);

  g_return_val_if_fail (IN_IS_PERSONAL_RESOURCE (self), 0);

  return priv->id;
}

void
in_personal_resource_set_id (InPersonalResource *self,
                             const gchar        *id)
{
  InPersonalResourcePrivate *priv = in_personal_resource_get_instance_private (self);

  g_return_if_fail (IN_IS_PERSONAL_RESOURCE (self));

  if (g_strcmp0 (priv->id, id) != 0)
    {
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ID]);
    }
}
