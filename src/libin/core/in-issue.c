/* in-issue.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-issue.h"
#include <json-glib/json-glib.h>

typedef struct
{
  gchar     *title;
  gchar     *description;
  gchar     *state;
  GDateTime *created_at;
  gchar     *web_url;
} InIssuePrivate;

static void in_issue_json_serializable_iface (JsonSerializableIface *iface);

G_DEFINE_TYPE_WITH_CODE (InIssue, in_issue, IN_TYPE_PERSONAL_RESOURCE,
                         G_ADD_PRIVATE (InIssue)
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE, in_issue_json_serializable_iface))

enum {
  PROP_0,
  PROP_TITLE,
  PROP_DESCRIPTION,
  PROP_STATE,
  PROP_CREATED_AT,
  PROP_WEB_URL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * in_issue_new:
 *
 * Create a new #InIssue.
 *
 * Returns: (transfer full): a newly created #InIssue
 */
InIssue *
in_issue_new (void)
{
  return g_object_new (IN_TYPE_ISSUE, NULL);
}

static void
in_issue_finalize (GObject *object)
{
  InIssue *self = (InIssue *)object;
  InIssuePrivate *priv = in_issue_get_instance_private (self);

  g_clear_pointer (&priv->title, g_free);
  g_clear_pointer (&priv->description, g_free);
  g_clear_pointer (&priv->state, g_free);
  g_clear_pointer (&priv->created_at, g_date_time_unref);
  g_clear_pointer (&priv->web_url, g_free);

  G_OBJECT_CLASS (in_issue_parent_class)->finalize (object);
}

static void
in_issue_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  InIssue *self = IN_ISSUE (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, in_issue_get_title (self));
      break;
    case PROP_DESCRIPTION:
      g_value_set_string (value, in_issue_get_description (self));
      break;
    case PROP_STATE:
      g_value_set_string (value, in_issue_get_state (self));
      break;
    case PROP_CREATED_AT:
      g_value_set_boxed (value, in_issue_get_created_at (self));
      break;
    case PROP_WEB_URL:
      g_value_set_string (value, in_issue_get_web_url (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_issue_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  InIssue *self = IN_ISSUE (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      in_issue_set_title (self, g_value_get_string (value));
      break;
    case PROP_DESCRIPTION:
      in_issue_set_description (self, g_value_get_string (value));
      break;
    case PROP_STATE:
      in_issue_set_state (self, g_value_get_string (value));
      break;
    case PROP_CREATED_AT:
      in_issue_set_created_at (self, g_value_get_boxed (value));
      break;
    case PROP_WEB_URL:
      in_issue_set_web_url (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_issue_class_init (InIssueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_issue_finalize;
  object_class->get_property = in_issue_get_property;
  object_class->set_property = in_issue_set_property;

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "Title",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "Description",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  properties [PROP_STATE] =
    g_param_spec_string ("state",
                         "State",
                         "State",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_CREATED_AT] =
    g_param_spec_boxed   ("created-at",
                          "CreatedAt",
                          "CreatedAt",
                          G_TYPE_DATE_TIME,
                          (G_PARAM_READWRITE |
                           G_PARAM_STATIC_STRINGS));

  properties [PROP_WEB_URL] =
    g_param_spec_string ("web-url",
                         "WebUrl",
                         "WebUrl",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
in_issue_init (InIssue *self)
{
}

const gchar *
in_issue_get_title (InIssue *self)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  g_return_val_if_fail (IN_IS_ISSUE (self), NULL);

  return priv->title;
}

void
in_issue_set_title (InIssue     *self,
                    const gchar *title)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);

  g_return_if_fail (IN_IS_ISSUE (self));

  if (g_strcmp0 (priv->title, title) != 0)
    {
      g_clear_pointer (&priv->title, g_free);
      priv->title = g_strdup (title);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TITLE]);
    }
}

const gchar *
in_issue_get_description (InIssue *self)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  g_return_val_if_fail (IN_IS_ISSUE (self), NULL);
  
  return priv->description;
}

void         
in_issue_set_description (InIssue     *self,
                          const gchar *description)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  
  g_return_if_fail (IN_IS_ISSUE (self));
  
  if (g_strcmp0 (priv->description, description) != 0)
    {
      g_clear_pointer (&priv->description, g_free);
      priv->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_DESCRIPTION]);
    }
}

const gchar *
in_issue_get_state (InIssue *self)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  g_return_val_if_fail (IN_IS_ISSUE (self), NULL);

  return priv->state;
}

void
in_issue_set_state (InIssue     *self,
                    const gchar *state)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);

  g_return_if_fail (IN_IS_ISSUE (self));

  if (g_strcmp0 (priv->state, state) != 0)
    {
      g_clear_pointer (&priv->state, g_free);
      priv->state = g_strdup (state);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_STATE]);
    }
}

GDateTime *
in_issue_get_created_at (InIssue *self)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  
  g_return_val_if_fail (IN_IS_ISSUE (self), NULL);
  
  return priv->created_at;
}

void         
in_issue_set_created_at (InIssue   *self,
                         GDateTime *created_at)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  
  g_return_if_fail (IN_IS_ISSUE (self));
  
  g_clear_pointer (&priv->created_at, g_date_time_unref);
  priv->created_at = g_date_time_ref (created_at);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CREATED_AT]);
}

const gchar *
in_issue_get_web_url (InIssue *self)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);
  g_return_val_if_fail (IN_IS_ISSUE (self), NULL);

  return priv->web_url;
}

void
in_issue_set_web_url (InIssue     *self,
                      const gchar *web_url)
{
  InIssuePrivate *priv = in_issue_get_instance_private (self);

  g_return_if_fail (IN_IS_ISSUE (self));

  if (g_strcmp0 (priv->web_url, web_url) != 0)
    {
      g_clear_pointer (&priv->web_url, g_free);
      priv->web_url = g_strdup (web_url);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_WEB_URL]);
    }
}

static gboolean
_deserialize_property (JsonSerializable *serializable,
                       const gchar      *property_name,
                       GValue           *value,
                       GParamSpec       *spec,
                       JsonNode         *property_node)
{
  if (g_strcmp0 (property_name, "created-at") == 0)
    {
      const gchar *created_at = json_node_get_string (property_node);
      GDateTime *dt = g_date_time_new_from_iso8601 (created_at, NULL);
      g_value_take_boxed (value, dt);
      return TRUE;
    }
  else if (g_strcmp0 (property_name, "id") == 0)
    {
      gchar buf[12];
      gint id = json_node_get_int (property_node);

      g_snprintf (buf, 11, "%d", id);
      g_value_set_string (value, buf);
      return TRUE;
    }

  return json_serializable_default_deserialize_property (serializable, property_name, value, spec, property_node);
}

static GParamSpec *
_find_property (JsonSerializable *serializable,
                const gchar      *name)
{
  if (g_strcmp0 (name, "iid") == 0)
    return g_object_class_find_property (G_OBJECT_GET_CLASS (serializable), "id");
  if (g_strcmp0 (name, "id") == 0)
    return NULL;
  return g_object_class_find_property (G_OBJECT_GET_CLASS (serializable), name);
}

static void
in_issue_json_serializable_iface (JsonSerializableIface *iface)
{
  iface->deserialize_property = _deserialize_property;
  iface->find_property = _find_property;
}
