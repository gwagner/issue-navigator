/* in-issue-provider.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-issue-provider.h"

G_DEFINE_INTERFACE (InIssueProvider, in_issue_provider, G_TYPE_OBJECT)

enum {
  UPDATED,
  RELOGIN_REQUIRED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

gboolean
in_issue_provider_default_login_required (InIssueProvider *self)
{
  g_return_val_if_fail (IN_IS_ISSUE_PROVIDER (self), FALSE);

  return FALSE;
}

static void
in_issue_provider_default_init (InIssueProviderInterface *iface)
{
  iface->login_required = in_issue_provider_default_login_required;
  signals [UPDATED] =
    g_signal_new ("updated",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);

  signals [RELOGIN_REQUIRED] =
    g_signal_new ("relogin-required",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);
}

void
in_issue_provider_load (InIssueProvider *self)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));
  
  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->load)
    IN_ISSUE_PROVIDER_GET_IFACE (self)->load (self);
}

gboolean
in_issue_provider_login_required (InIssueProvider *self)
{
  g_return_val_if_fail (IN_IS_ISSUE_PROVIDER (self), FALSE);

  return IN_ISSUE_PROVIDER_GET_IFACE (self)->login_required (self);
}

void
in_issue_provider_login (InIssueProvider *self,
                         GtkDialog       *dialog)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));

  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->login)
    IN_ISSUE_PROVIDER_GET_IFACE (self)->login (self, dialog);
}

void
in_issue_provider_fetch_issues (InIssueProvider *self)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));

  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_issues)
    IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_issues (self);
}

/**
 * in_issue_provider_fetch_issues_async:
 * @self: an #InIssueProvider
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 */
void
in_issue_provider_fetch_issues_async (InIssueProvider     *self,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_issues_async)
    IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_issues_async (self, cancellable, callback, user_data);
}

gboolean
in_issue_provider_fetch_issues_finish (InIssueProvider  *self,
                                       GAsyncResult     *result,
                                       GError          **error)
{
  g_return_val_if_fail (IN_IS_ISSUE_PROVIDER (self), FALSE);

  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_issues_finish)
    IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_issues_finish (self, result, error);

  return FALSE;
}

/**
 * in_issue_provider_fetch_comments_async:
 * @self: an #InIssueProvider
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 */
void
in_issue_provider_fetch_comments_async (InIssueProvider     *self,
                                        InIssue             *issue,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_comments_async)
    IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_comments_async (self, issue, cancellable, callback, user_data);
}

/**
 * in_issue_provider_fetch_comments_finish:
 * @self: an #InIssueProvider
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns:
 */
gboolean
in_issue_provider_fetch_comments_finish (InIssueProvider  *self,
                                         GAsyncResult     *result,
                                         GPtrArray       **comments,
                                         GError          **error)
{
  g_return_val_if_fail (IN_IS_ISSUE_PROVIDER (self), FALSE);

  if (IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_comments_finish)
    return IN_ISSUE_PROVIDER_GET_IFACE (self)->fetch_comments_finish (self, result, comments, error);
  return FALSE;
}

static gboolean
emit_signal_on_main_thread (gpointer user_data)
{
  InIssueProvider *self = IN_ISSUE_PROVIDER (user_data);

  g_return_val_if_fail (IN_IS_ISSUE_PROVIDER (self), G_SOURCE_REMOVE);

  g_signal_emit (self, signals [UPDATED], 0);
  
  return G_SOURCE_REMOVE;
}

void
in_issue_provider_emit_updated (InIssueProvider *self)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));

  g_idle_add (emit_signal_on_main_thread, self);
}

static gboolean
emit_signal2_on_main_thread (gpointer user_data)
{
  InIssueProvider *self = IN_ISSUE_PROVIDER (user_data);

  g_return_val_if_fail (IN_IS_ISSUE_PROVIDER (self), G_SOURCE_REMOVE);

  g_signal_emit (self, signals [RELOGIN_REQUIRED], 0);

  return G_SOURCE_REMOVE;
}


void
in_issue_provider_emit_relogin_required (InIssueProvider *self)
{
  g_return_if_fail (IN_IS_ISSUE_PROVIDER (self));

  g_signal_emit (self, signals [RELOGIN_REQUIRED], 0);

  /* g_idle_add (emit_signal2_on_main_thread, (self)); */
}
