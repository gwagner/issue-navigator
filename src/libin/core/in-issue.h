/* in-issue.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "in-personal-resource.h"

G_BEGIN_DECLS

#define IN_TYPE_ISSUE (in_issue_get_type())

G_DECLARE_DERIVABLE_TYPE (InIssue, in_issue, IN, ISSUE, InPersonalResource)

struct _InIssueClass
{
  InPersonalResourceClass parent_class;
};

InIssue     *in_issue_new             (void);
const gchar *in_issue_get_title       (InIssue     *self);
void         in_issue_set_title       (InIssue     *self,
                                       const gchar *title);
const gchar *in_issue_get_description (InIssue     *self);
void         in_issue_set_description (InIssue     *self,
                                       const gchar *description);
const gchar *in_issue_get_state       (InIssue     *self);
void         in_issue_set_state       (InIssue     *self,
                                       const gchar *state);
GDateTime   *in_issue_get_created_at  (InIssue     *self);
void         in_issue_set_created_at  (InIssue     *self,
                                       GDateTime   *created_at);
const gchar *in_issue_get_web_url     (InIssue     *self);
void         in_issue_set_web_url     (InIssue     *self,
                                       const gchar *web_url);
G_END_DECLS
