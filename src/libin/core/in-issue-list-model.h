/* in-issue-list-model.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "in-issue.h"

G_BEGIN_DECLS

#define IN_TYPE_ISSUE_LIST_MODEL (in_issue_list_model_get_type())

G_DECLARE_FINAL_TYPE (InIssueListModel, in_issue_list_model, IN, ISSUE_LIST_MODEL, GObject)

InIssueListModel *in_issue_list_model_new (void);
void              in_issue_list_model_add (InIssueListModel *self,
                                           InIssue          *issue);

G_END_DECLS
