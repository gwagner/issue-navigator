/* in-pango-parser.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-pango-parser.h"

G_DEFINE_INTERFACE (InPangoParser, in_pango_parser, G_TYPE_OBJECT)

static void
in_pango_parser_default_init (InPangoParserInterface *iface)
{
}

/**
 * in_pango_parser_parse:
 * @self: a #InPangoParser
 * @text: the string to parse
 *
 * Returns: (transfer full): the parsed pango markup
 */
GtkWidget *
in_pango_parser_parse (InPangoParser *self,
                       const gchar   *text)
{
  g_return_val_if_fail (IN_IS_PANGO_PARSER (self), NULL);
  g_return_val_if_fail (text != NULL, NULL);

  return IN_PANGO_PARSER_GET_IFACE (self)->parse (self, text);
}
