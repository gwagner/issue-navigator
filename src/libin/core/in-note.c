/* in-note.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-note.h"
#include <json-glib/json-glib.h>

struct _InNote
{
  InPersonalResource parent_instance;
  
  gint parent_id;
  gchar *body;
  GDateTime *created_at;
};

static void in_note_json_serializable_iface (JsonSerializableIface *iface);

G_DEFINE_TYPE_WITH_CODE (InNote, in_note, IN_TYPE_PERSONAL_RESOURCE,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE, in_note_json_serializable_iface))

enum {
  PROP_0,
  PROP_PARENT_ID,
  PROP_BODY,
  PROP_CREATED_AT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

InNote *
in_note_new (void)
{
  return g_object_new (IN_TYPE_NOTE, NULL);
}

static void
in_note_finalize (GObject *object)
{
  InNote *self = (InNote *)object;

  g_clear_pointer (&self->body, g_free);
  g_clear_pointer (&self->created_at, g_date_time_unref);
  
  G_OBJECT_CLASS (in_note_parent_class)->finalize (object);
}

static void
in_note_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  InNote *self = IN_NOTE (object);

  switch (prop_id)
    {
    case PROP_PARENT_ID:
      g_value_set_int (value, in_note_get_parent_id (self));
      break;
    case PROP_BODY:
      g_value_set_string (value, in_note_get_body (self));
      break;
    case PROP_CREATED_AT:
      g_value_set_boxed (value, in_note_get_created_at (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_note_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  InNote *self = IN_NOTE (object);

  switch (prop_id)
    {
    case PROP_PARENT_ID:
      in_note_set_parent_id (self, g_value_get_int (value));
      break;
    case PROP_BODY:
      in_note_set_body (self, g_value_get_string (value));
      break;
    case PROP_CREATED_AT:
      in_note_set_created_at (self, g_value_get_boxed (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_note_class_init (InNoteClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_note_finalize;
  object_class->get_property = in_note_get_property;
  object_class->set_property = in_note_set_property;
  
  properties [PROP_PARENT_ID] =
    g_param_spec_int ("parent-id",
                      "ParentId",
                      "ParentId",
                      0,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE |
                       G_PARAM_STATIC_STRINGS));

  properties [PROP_BODY] =
    g_param_spec_string ("body",
                         "Body",
                         "Body",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
  
  properties [PROP_CREATED_AT] =
    g_param_spec_boxed ("created-at",
                        "CreatedAt",
                        "CreatedAt",
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
in_note_init (InNote *self)
{
}

gint
in_note_get_parent_id (InNote *self)
{
  g_return_val_if_fail (IN_IS_NOTE (self), 0);

  return self->parent_id;
}

void
in_note_set_parent_id (InNote *self,
                       gint    parent_id)
{
  g_return_if_fail (IN_IS_NOTE (self));

  self->parent_id = parent_id;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_PARENT_ID]);
}

const gchar *
in_note_get_body (InNote *self)
{
  g_return_val_if_fail (IN_IS_NOTE (self), NULL);
  
  return self->body;
}

void         
in_note_set_body (InNote      *self,
                  const gchar *body)
{
  g_return_if_fail (IN_IS_NOTE (self));
  
  if (g_strcmp0 (self->body, body) != 0)
    {
      g_clear_pointer (&self->body, g_free);
      self->body = g_strdup (body);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_BODY]);
    }
}

static gboolean
_deserialize_property (JsonSerializable *serializable,
                       const gchar      *property_name,
                       GValue           *value,
                       GParamSpec       *spec,
                       JsonNode         *property_node)
{
  if (g_strcmp0 (property_name, "created-at") == 0)
    {
      const gchar *created_at = json_node_get_string (property_node);
      GDateTime *dt = g_date_time_new_from_iso8601 (created_at, NULL);
      g_value_take_boxed (value, dt);
      return TRUE;
    }
  else if (g_strcmp0 (property_name, "id") == 0)
    {
      gchar buf[12];
      gint id = json_node_get_int (property_node);

      g_snprintf (buf, 11, "%d", id);
      g_value_set_string (value, buf);
      return TRUE;
    }
  return json_serializable_default_deserialize_property (serializable, property_name, value, spec, property_node);
}


static void
in_note_json_serializable_iface (JsonSerializableIface *iface)
{
  iface->deserialize_property = _deserialize_property;
}

GDateTime *
in_note_get_created_at (InNote *self)
{
  g_return_val_if_fail (IN_IS_NOTE (self), NULL);

  return self->created_at;
}

void
in_note_set_created_at (InNote    *self,
                        GDateTime *created_at)
{
  g_return_if_fail (IN_IS_NOTE (self));

  g_clear_pointer (&self->created_at, g_date_time_unref);
  self->created_at = g_date_time_ref (created_at);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CREATED_AT]);
}
