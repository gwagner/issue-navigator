/* in-issue-provider.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>
#include <gtk/gtk.h>
#include "in-issue.h"

G_BEGIN_DECLS

#define IN_TYPE_ISSUE_PROVIDER (in_issue_provider_get_type ())

G_DECLARE_INTERFACE (InIssueProvider, in_issue_provider, IN, ISSUE_PROVIDER, GObject)

struct _InIssueProviderInterface
{
  GTypeInterface parent;

  void       (*load)                  (InIssueProvider      *self);
  gboolean   (*login_required)        (InIssueProvider      *self);
  void       (*login)                 (InIssueProvider      *self,
                                       GtkDialog            *dialog);
  void       (*fetch_issues)          (InIssueProvider      *self);
  void       (*fetch_issues_async)    (InIssueProvider      *self,
                                       GCancellable         *cancellable,
                                       GAsyncReadyCallback   callback,
                                       gpointer              user_data);
  gboolean   (*fetch_issues_finish)   (InIssueProvider  *self,
                                       GAsyncResult     *result,
                                       GError          **error);
  void       (*fetch_comments_async)  (InIssueProvider     *self,
                                       InIssue             *issue,
                                       GCancellable        *cancellable,
                                       GAsyncReadyCallback  callback,
                                       gpointer             user_data);
  gboolean   (*fetch_comments_finish) (InIssueProvider  *self,
                                       GAsyncResult     *result,
                                       GPtrArray       **comments,
                                       GError          **error);
};

void     in_issue_provider_load                  (InIssueProvider      *self);
gboolean in_issue_provider_login_required        (InIssueProvider      *self);
void     in_issue_provider_login                 (InIssueProvider      *self,
                                                  GtkDialog            *dialog);
void     in_issue_provider_fetch_issues          (InIssueProvider      *self);
void     in_issue_provider_fetch_issues_async    (InIssueProvider      *self,
                                                  GCancellable         *cancellable,
                                                  GAsyncReadyCallback   callback,
                                                  gpointer              user_data);
gboolean in_issue_provider_fetch_issues_finish   (InIssueProvider      *self,
                                                  GAsyncResult         *result,
                                                  GError              **error);
void     in_issue_provider_emit_updated          (InIssueProvider      *self);

void     in_issue_provider_emit_relogin_required (InIssueProvider      *self);
void     in_issue_provider_fetch_comments_async  (InIssueProvider      *self,
                                                  InIssue              *issue,
                                                  GCancellable         *cancellable,
                                                  GAsyncReadyCallback   callback,
                                                  gpointer              user_data);
gboolean in_issue_provider_fetch_comments_finish (InIssueProvider      *self,
                                                  GAsyncResult         *result,
                                                  GPtrArray           **comments,
                                                  GError              **error);

G_END_DECLS
