/* in-note.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "in-personal-resource.h"

G_BEGIN_DECLS

#define IN_TYPE_NOTE (in_note_get_type())

G_DECLARE_FINAL_TYPE (InNote, in_note, IN, NOTE, InPersonalResource)

InNote      *in_note_new            (void);
gint         in_note_get_parent_id  (InNote      *self);
void         in_note_set_parent_id  (InNote      *self,
                                     gint         parent_id);
const gchar *in_note_get_body       (InNote      *self);
void         in_note_set_body       (InNote      *self,
                                     const gchar *body);
GDateTime   *in_note_get_created_at (InNote      *self);
void         in_note_set_created_at (InNote      *self,
                                     GDateTime   *created_at);
G_END_DECLS
