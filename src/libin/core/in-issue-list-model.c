/* in-issue-list-model.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-issue-list-model.h"
#include <gio/gio.h>

struct _InIssueListModel
{
  GObject parent_instance;
  GPtrArray *items;
};

void in_issue_list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (InIssueListModel, in_issue_list_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, in_issue_list_model_iface_init))

InIssueListModel *
in_issue_list_model_new (void)
{
  return g_object_new (IN_TYPE_ISSUE_LIST_MODEL, NULL);
}

static void
in_issue_list_model_finalize (GObject *object)
{
  InIssueListModel *self = (InIssueListModel *)object;

  g_ptr_array_unref (self->items);

  G_OBJECT_CLASS (in_issue_list_model_parent_class)->finalize (object);
}

static void
in_issue_list_model_class_init (InIssueListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_issue_list_model_finalize;
}

static void
in_issue_list_model_init (InIssueListModel *self)
{
  self->items = g_ptr_array_new_with_free_func (g_object_unref);
}

static gint
in_issue_list_model_contains_issue (InIssueListModel *self,
                                    const gchar      *id)
{
  g_return_val_if_fail (IN_IS_ISSUE_LIST_MODEL (self), FALSE);

  for (guint i = 0; i < self->items->len; i++)
    {
      InIssue *issue = g_ptr_array_index (self->items, i);
      if (g_strcmp0 (in_personal_resource_get_id (IN_PERSONAL_RESOURCE (issue)), id) == 0)
        return i;
    }
  return -1;
}

static void
in_issue_list_model_merge_issue (InIssueListModel *self,
                                 InIssue          *issue,
                                 gint              issue_index)
{
  InIssue *other = NULL;

  g_return_if_fail (IN_IS_ISSUE_LIST_MODEL (self));
  g_return_if_fail (IN_IS_ISSUE (issue));
  g_return_if_fail (issue_index >= 0);

  other = g_ptr_array_index (self->items, issue_index);

  if (g_strcmp0 (in_issue_get_title (issue), in_issue_get_title (other)) != 0)
      in_issue_set_title (other, in_issue_get_title (issue));
  if (g_strcmp0 (in_issue_get_description (issue), in_issue_get_description (other)) != 0)
      in_issue_set_description (other, in_issue_get_description (issue));
  if (g_strcmp0 (in_issue_get_state (issue), in_issue_get_state (other)) != 0)
      in_issue_set_state (other, in_issue_get_state (issue));
  if (g_strcmp0 (in_issue_get_web_url (issue), in_issue_get_web_url (other)) != 0)
      in_issue_set_web_url (other, in_issue_get_web_url (issue));
  // TODO: created at is not merged
}

/* static gint */
/* in_issue_list_model_compare (gconstpointer a, */
/*                              gconstpointer b) */
/* { */
/*   InIssue *issue1 = *((InIssue **) a); */
/*   InIssue *issue2 = *((InIssue **) b); */

/*   return g_date_time_compare (in_issue_get_created_at (issue1), in_issue_get_created_at (issue2)); */
/* } */

void
in_issue_list_model_add (InIssueListModel *self,
                         InIssue          *issue)
{
  gint issue_index;

  g_return_if_fail (IN_IS_ISSUE_LIST_MODEL (self));
  g_return_if_fail (IN_IS_ISSUE (issue));

  // check if issue is already in list
  issue_index = in_issue_list_model_contains_issue (self, in_personal_resource_get_id (IN_PERSONAL_RESOURCE (issue)));
  if (issue_index == -1)
    {
      g_ptr_array_add (self->items, g_object_ref (issue));
      g_list_model_items_changed (G_LIST_MODEL (self), self->items->len-1, 0, 1);
    }
  else
    {
      in_issue_list_model_merge_issue (self, issue, issue_index);
    }

  g_object_unref (issue);
}

static GType
in_issue_list_model_get_item_type (GListModel *model)
{
  InIssueListModel *self = IN_ISSUE_LIST_MODEL (model);

  g_return_val_if_fail (IN_IS_ISSUE_LIST_MODEL (self), 0);

  return IN_TYPE_ISSUE;
}

static guint
in_issue_list_model_get_n_items (GListModel *model)
{
  InIssueListModel *self = IN_ISSUE_LIST_MODEL (model);

  g_return_val_if_fail (IN_IS_ISSUE_LIST_MODEL (self), 0);

  return self->items->len;
}

static gpointer
in_issue_list_model_get_item (GListModel *model,
                              guint       position)
{
  InIssueListModel *self = IN_ISSUE_LIST_MODEL (model);

  g_return_val_if_fail (IN_IS_ISSUE_LIST_MODEL (self), NULL);

  if (self->items->len == 0)
    return NULL;

  return g_object_ref (g_ptr_array_index (self->items, position));
}

void
in_issue_list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item = in_issue_list_model_get_item;
  iface->get_n_items = in_issue_list_model_get_n_items;
  iface->get_item_type = in_issue_list_model_get_item_type;
}
