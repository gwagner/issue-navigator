/* in-personal-resource.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define IN_TYPE_PERSONAL_RESOURCE (in_personal_resource_get_type())

G_DECLARE_DERIVABLE_TYPE (InPersonalResource, in_personal_resource, IN, PERSONAL_RESOURCE, GObject)

struct _InPersonalResourceClass
{
  GObjectClass parent_class;
};

InPersonalResource *in_personal_resource_new           (void);
const gchar        *in_personal_resource_get_id        (InPersonalResource *self);
void                in_personal_resource_set_id        (InPersonalResource *self,
                                                        const gchar        *id);
const gchar        *in_personal_resource_get_author_id (InPersonalResource *self);
void                in_personal_resource_set_author_id (InPersonalResource *self,
                                                        const gchar        *author_id);

G_END_DECLS
