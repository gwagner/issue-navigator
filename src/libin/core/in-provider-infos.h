/* in-provider-infos.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define IN_TYPE_PROVIDER_INFOS (in_provider_infos_get_type())

G_DECLARE_FINAL_TYPE (InProviderInfos, in_provider_infos, IN, PROVIDER_INFOS, GObject)

InProviderInfos *in_provider_infos_new               (void);
const gchar     *in_provider_infos_get_name          (InProviderInfos *self);
void             in_provider_infos_set_name          (InProviderInfos *self,
                                                      const gchar     *name);
const gchar     *in_provider_infos_get_url           (InProviderInfos *self);
void             in_provider_infos_set_url           (InProviderInfos *self,
                                                      const gchar     *url);
const gchar     *in_provider_infos_get_client_id     (InProviderInfos *self);
void             in_provider_infos_set_client_id     (InProviderInfos *self,
                                                      const gchar     *client_id);
const gchar     *in_provider_infos_get_client_secret (InProviderInfos *self);
void             in_provider_infos_set_client_secret (InProviderInfos *self,
                                                      const gchar     *client_secret);
const gchar     *in_provider_infos_get_access_token  (InProviderInfos *self);
void             in_provider_infos_set_access_token  (InProviderInfos *self,
                                                      const gchar     *access_token);
const gchar     *in_provider_infos_get_refresh_token (InProviderInfos *self);
void             in_provider_infos_set_refresh_token (InProviderInfos *self,
                                                      const gchar     *refresh_token);
GDateTime       *in_provider_infos_get_expiration_date (InProviderInfos *self);
void             in_provider_infos_set_expiration_date (InProviderInfos *self,
                                                        GDateTime       *expiration_date);
G_END_DECLS
