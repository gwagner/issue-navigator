/* in-provider-infos.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-provider-infos.h"

struct _InProviderInfos
{
  GObject parent_instance;

  gchar *name;
  gchar *url;
  gchar *client_id;
  gchar *client_secret;
  gchar *access_token;
  gchar *refresh_token;
  GDateTime *expiration_date;
};

G_DEFINE_FINAL_TYPE (InProviderInfos, in_provider_infos, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_URL,
  PROP_CLIENT_ID,
  PROP_CLIENT_SECRET,
  PROP_ACCESS_TOKEN,
  PROP_REFRESH_TOKEN,
  PROP_EXPIRATION_DATE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

InProviderInfos *
in_provider_infos_new (void)
{
  return g_object_new (IN_TYPE_PROVIDER_INFOS, NULL);
}

static void
in_provider_infos_finalize (GObject *object)
{
  InProviderInfos *self = (InProviderInfos *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->url, g_free);
  g_clear_pointer (&self->client_id, g_free);
  g_clear_pointer (&self->client_secret, g_free);
  g_clear_pointer (&self->access_token, g_free);
  g_clear_pointer (&self->refresh_token, g_free);
  g_clear_pointer (&self->expiration_date, g_date_time_unref);

  G_OBJECT_CLASS (in_provider_infos_parent_class)->finalize (object);
}

static void
in_provider_infos_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  InProviderInfos *self = IN_PROVIDER_INFOS (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, in_provider_infos_get_name (self));
      break;
    case PROP_URL:
      g_value_set_string (value, in_provider_infos_get_url (self));
      break;
    case PROP_CLIENT_ID:
      g_value_set_string (value, in_provider_infos_get_client_id (self));
      break;
    case PROP_CLIENT_SECRET:
      g_value_set_string (value, in_provider_infos_get_client_secret (self));
      break;
    case PROP_ACCESS_TOKEN:
      g_value_set_string (value, in_provider_infos_get_access_token (self));
      break;
    case PROP_REFRESH_TOKEN:
      g_value_set_string (value, in_provider_infos_get_refresh_token (self));
      break;
    case PROP_EXPIRATION_DATE:
      g_value_set_boxed (value, in_provider_infos_get_expiration_date (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_provider_infos_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  InProviderInfos *self = IN_PROVIDER_INFOS (object);

  switch (prop_id)
    {
    case PROP_NAME:
      in_provider_infos_set_name (self, g_value_get_string (value));
      break;
    case PROP_URL:
      in_provider_infos_set_url (self, g_value_get_string (value));
      break;
    case PROP_CLIENT_ID:
      in_provider_infos_set_client_id (self, g_value_get_string (value));
      break;
    case PROP_CLIENT_SECRET:
      in_provider_infos_set_client_secret (self, g_value_get_string (value));
      break;
    case PROP_ACCESS_TOKEN:
      in_provider_infos_set_access_token (self, g_value_get_string (value));
      break;
    case PROP_REFRESH_TOKEN:
      in_provider_infos_set_refresh_token (self, g_value_get_string (value));
      break;
    case PROP_EXPIRATION_DATE:
      in_provider_infos_set_expiration_date (self, g_value_get_boxed (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_provider_infos_class_init (InProviderInfosClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_provider_infos_finalize;
  object_class->get_property = in_provider_infos_get_property;
  object_class->set_property = in_provider_infos_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "Name",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_URL] =
    g_param_spec_string ("url",
                         "Url",
                         "Url",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_CLIENT_ID] =
    g_param_spec_string ("client-id",
                         "ClientId",
                         "ClientId",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_CLIENT_SECRET] =
    g_param_spec_string ("client-secret",
                         "ClientSecret",
                         "ClientSecret",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_ACCESS_TOKEN] =
    g_param_spec_string ("access-token",
                         "AccessToken",
                         "AccessToken",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_REFRESH_TOKEN] =
    g_param_spec_string ("refresh-token",
                         "RefreshToken",
                         "RefreshToken",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_EXPIRATION_DATE] =
    g_param_spec_boxed ("expiration-date",
                        "ExpirationDate",
                        "ExpirationDate",
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
in_provider_infos_init (InProviderInfos *self)
{
}

const gchar *
in_provider_infos_get_name (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->name;
}

void
in_provider_infos_set_name (InProviderInfos *self,
                            const gchar     *name)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  if (g_strcmp0 (self->name, name) != 0)
    {
      g_clear_pointer (&self->name, g_free);
      self->name = g_strdup (name),
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
    }
}

const gchar *
in_provider_infos_get_url (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->url;
}

void
in_provider_infos_set_url (InProviderInfos *self,
                           const gchar     *url)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  if (g_strcmp0 (self->url, url) != 0)
    {
      g_clear_pointer (&self->url, g_free);
      self->url = g_strdup (url),
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_URL]);
    }
}

const gchar *
in_provider_infos_get_client_id (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->client_id;
}

void
in_provider_infos_set_client_id (InProviderInfos *self,
                                 const gchar     *client_id)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  if (g_strcmp0 (self->client_id, client_id) != 0)
    {
      g_clear_pointer (&self->client_id, g_free);
      self->client_id = g_strdup (client_id),
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CLIENT_ID]);
    }
}

const gchar *
in_provider_infos_get_client_secret (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->client_secret;
}

void
in_provider_infos_set_client_secret (InProviderInfos *self,
                                     const gchar     *client_secret)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  if (g_strcmp0 (self->client_secret, client_secret) != 0)
    {
      g_clear_pointer (&self->client_secret, g_free);
      self->client_secret = g_strdup (client_secret),
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CLIENT_SECRET]);
    }
}

const gchar *
in_provider_infos_get_access_token (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->access_token;
}

void
in_provider_infos_set_access_token (InProviderInfos *self,
                                    const gchar     *access_token)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  if (g_strcmp0 (self->access_token, access_token) != 0)
    {
      g_clear_pointer (&self->access_token, g_free);
      self->access_token = g_strdup (access_token),
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ACCESS_TOKEN]);
    }
}

const gchar *
in_provider_infos_get_refresh_token (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->refresh_token;
}

void
in_provider_infos_set_refresh_token (InProviderInfos *self,
                                     const gchar     *refresh_token)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  if (g_strcmp0 (self->refresh_token, refresh_token) != 0)
    {
      g_clear_pointer (&self->refresh_token, g_free);
      self->refresh_token = g_strdup (refresh_token),
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_REFRESH_TOKEN]);
    }
}

GDateTime *
in_provider_infos_get_expiration_date (InProviderInfos *self)
{
  g_return_val_if_fail (IN_IS_PROVIDER_INFOS (self), NULL);

  return self->expiration_date;
}

void
in_provider_infos_set_expiration_date (InProviderInfos *self,
                                       GDateTime       *expiration_date)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS (self));

  g_clear_pointer (&self->expiration_date, g_date_time_unref);
  self->expiration_date = g_date_time_ref (expiration_date);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_EXPIRATION_DATE]);
}
