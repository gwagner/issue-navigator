/* in-pango-parser.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define IN_TYPE_PANGO_PARSER (in_pango_parser_get_type ())

G_DECLARE_INTERFACE (InPangoParser, in_pango_parser, IN, PANGO_PARSER, GObject)

struct _InPangoParserInterface
{
  GTypeInterface parent;

  GtkWidget *(*parse) (InPangoParser *self, const gchar *text);
};

GtkWidget *in_pango_parser_parse (InPangoParser *self,
                              const gchar   *text);
G_END_DECLS
