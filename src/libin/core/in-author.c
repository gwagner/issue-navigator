/* in-author.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-author.h"
#include <json-glib/json-glib.h>

struct _InAuthor
{
  GObject parent_instance;

  gchar *id;
  gchar *name;
  GdkTexture *image;
};

static void in_author_json_serializable_iface (JsonSerializableIface *iface);

G_DEFINE_TYPE_WITH_CODE (InAuthor, in_author, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE, in_author_json_serializable_iface))

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_IMAGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

InAuthor *
in_author_new (void)
{
  return g_object_new (IN_TYPE_AUTHOR, NULL);
}

static void
in_author_finalize (GObject *object)
{
  InAuthor *self = (InAuthor *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->image, g_object_unref);

  G_OBJECT_CLASS (in_author_parent_class)->finalize (object);
}

static void
in_author_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  InAuthor *self = IN_AUTHOR (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, in_author_get_id (self));
      break;
    case PROP_NAME:
      g_value_set_string (value, in_author_get_name (self));
      break;
    case PROP_IMAGE:
      g_value_set_object (value, in_author_get_image (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_author_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  InAuthor *self = IN_AUTHOR (object);

  switch (prop_id)
    {
    case PROP_ID:
      in_author_set_id (self, g_value_get_string (value));
      break;
    case PROP_NAME:
      in_author_set_name (self, g_value_get_string (value));
      break;
    case PROP_IMAGE:
      in_author_set_image (self, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_author_class_init (InAuthorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_author_finalize;
  object_class->get_property = in_author_get_property;
  object_class->set_property = in_author_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "Id",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "Name",
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_IMAGE] =
    g_param_spec_object ("image",
                         "Image",
                         "Image",
                         GDK_TYPE_TEXTURE,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
in_author_init (InAuthor *self)
{
}

const gchar *
in_author_get_id (InAuthor *self)
{
  g_return_val_if_fail (IN_IS_AUTHOR (self), NULL);

  return self->id;
}

void
in_author_set_id (InAuthor    *self,
                  const gchar *id)
{
  g_return_if_fail (IN_IS_AUTHOR (self));

  if (g_strcmp0 (self->id, id) != 0)
    {
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ID]);
    }
}

const gchar *
in_author_get_name (InAuthor *self)
{
  g_return_val_if_fail (IN_IS_AUTHOR (self), NULL);

  return self->name;
}

void
in_author_set_name (InAuthor    *self,
                    const gchar *name)
{
  g_return_if_fail (IN_IS_AUTHOR (self));

  if (g_strcmp0 (self->name, name) != 0)
    {
      g_clear_pointer (&self->name, g_free);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
    }
}

/**
 * in_author_get_image:
 * @self: a #InAuthor
 *
 * Returns: (transfer none): a #GdkTexture
 */
GdkTexture *
in_author_get_image (InAuthor *self)
{
  g_return_val_if_fail (IN_IS_AUTHOR (self), NULL);

  return self->image;
}

void
in_author_set_image (InAuthor   *self,
                     GdkTexture *image)
{
  g_return_if_fail (IN_IS_AUTHOR (self));

  if (image)
    {
      g_clear_object (&self->image);
      self->image = g_object_ref (image);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_IMAGE]);
    }
}


static gboolean
_deserialize_property (JsonSerializable *serializable,
                       const gchar      *property_name,
                       GValue           *value,
                       GParamSpec       *spec,
                       JsonNode         *property_node)
{
  if (g_strcmp0 (property_name, "id") == 0)
    {
      gchar buf[12];
      gint id = json_node_get_int (property_node);

      g_snprintf (buf, 11, "%d", id);
      g_value_set_string (value, buf);
      return TRUE;
    }
  return json_serializable_default_deserialize_property (serializable, property_name, value, spec, property_node);
}

static void
in_author_json_serializable_iface (JsonSerializableIface *iface)
{
  iface->deserialize_property = _deserialize_property;
}
