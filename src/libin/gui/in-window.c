/* is-window.c
 *
 * Copyright 2021 Günther Wagner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "in-config.h"
#include "in-window.h"
#include "in-issue.h"
#include "in-issue-provider.h"
#include "in-issue-page.h"
#include "in-application.h"
#include "in-issue-service.h"
#include "in-issue-list-model.h"
#include <libpeas/peas.h>

static GThread *main_thread;

struct _InWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkStack            *stack;
  GtkStack            *details_stack;
  GtkRevealer         *search_revealer;
  GtkScrolledWindow   *list_view_sw;
  GtkListView         *list_view;
  InIssuePage         *issue_page;
  GtkSearchEntry      *search_entry;
  GtkRevealer         *backend_revealer;
  GtkSpinner          *backend_spinner;

  GtkFilter           *string_filter;
  InIssueListModel    *model;

  PeasExtensionSet    *issue_provider;
};

G_DEFINE_TYPE (InWindow, in_window, GTK_TYPE_APPLICATION_WINDOW)

static void in_window_load (InWindow *self);
static void backend_start (InIssueService *service,
                           gpointer        user_data);
static void backend_stop  (InIssueService *service,
                           gpointer        user_data);

static void
in_window_login_provider (InIssueProvider *provider,
                          gpointer         user_data)
{
  InWindow *self = (InWindow *)user_data;

  g_assert (g_thread_self () == main_thread);
  g_assert (IN_IS_WINDOW (self));

  if (in_issue_provider_login_required (provider))
    {
      /* GtkWidget *dialog = gtk_dialog_new (); */

      GtkDialog *dialog = GTK_DIALOG (gtk_dialog_new_with_buttons ("Title", GTK_WINDOW (self), GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_USE_HEADER_BAR | GTK_DIALOG_MODAL, "Ok", GTK_RESPONSE_OK, "Cancel", GTK_RESPONSE_CANCEL, NULL));
      gtk_dialog_set_default_response (dialog, GTK_RESPONSE_OK);
      in_issue_provider_login (provider, dialog);

      gtk_widget_show (GTK_WIDGET (dialog));
    }
}

static void
in_window_issue_provider_added (PeasExtensionSet *set,
                                PeasPluginInfo   *plugin_info,
                                PeasExtension    *exten,
                                gpointer          user_data)
{
  InWindow *self = (InWindow *)user_data;
  InIssueProvider *provider = (InIssueProvider *)exten;

  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (plugin_info != NULL);
  g_assert (IN_IS_ISSUE_PROVIDER (provider));

  in_issue_provider_load (provider);

  /* g_signal_connect (provider, "relogin-required", G_CALLBACK (in_window_login_provider), self); */
  g_signal_connect_object (provider, "relogin-required", G_CALLBACK (in_window_login_provider), self, G_CONNECT_AFTER);

  in_window_login_provider (provider, self);
}

static void
in_window_constructed (GObject *object)
{
  InWindow *self = IN_WINDOW (object);
  InIssueService *service = NULL;

  G_OBJECT_CLASS (in_window_parent_class)->constructed (object);

  self->issue_provider = peas_extension_set_new (NULL, IN_TYPE_ISSUE_PROVIDER, NULL);

  service = in_issue_service_get_default ();
  in_issue_service_set_provider (service, self->issue_provider);
  g_signal_connect_object (service, "start", G_CALLBACK (backend_start), self, 0);
  g_signal_connect_object (service, "stop", G_CALLBACK (backend_stop), self, 0);
  in_issue_service_connect (service, G_CALLBACK (in_window_load), self);
}

static void
in_window_show (GtkWidget *widget)
{
  InWindow *self = (InWindow *)widget;

  GTK_WIDGET_CLASS (in_window_parent_class)->show (widget);

  peas_extension_set_foreach (self->issue_provider, in_window_issue_provider_added, self);
}

static void
in_window_issue_activated (GtkListView *list,
                           guint        pos,
                           gpointer     user_data)
{
  InWindow *self = IN_WINDOW (user_data);
  InIssueService *service = NULL;
  InIssue *issue;
  
  service = in_issue_service_get_default ();

  issue = g_list_model_get_item (G_LIST_MODEL (gtk_list_view_get_model (list)), pos);
  in_issue_page_set_issue (self->issue_page, issue);

  in_issue_service_get_comments (service, issue, in_issue_page_comments_fetched, self->issue_page);

  gtk_stack_set_visible_child_name (self->details_stack, "details");
}

static void
in_window_search_toggled (GtkToggleButton *btn,
                          gpointer         user_data)
{
  InWindow *self = IN_WINDOW (user_data);
  gboolean active = FALSE;

  g_return_if_fail (IN_IS_WINDOW (self));

  active = gtk_toggle_button_get_active (btn);
  if (!active)
    gtk_editable_set_text (GTK_EDITABLE (self->search_entry), "");

  gtk_revealer_set_reveal_child (self->search_revealer, active);
}

static void
in_window_search_changed (GtkSearchEntry *entry,
                          gpointer        user_data)
{
  InWindow *self = IN_WINDOW (user_data);
  const gchar *searchterm;

  g_return_if_fail (IN_IS_WINDOW (self));

  searchterm = gtk_editable_get_text (GTK_EDITABLE (entry));
  gtk_string_filter_set_search (GTK_STRING_FILTER (self->string_filter), searchterm);
}

static void
in_window_class_init (InWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = in_window_constructed;
  widget_class->show = in_window_show;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/IssueNavigator/gui/in-window.ui");
  gtk_widget_class_bind_template_child (widget_class, InWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, InWindow, list_view);
  gtk_widget_class_bind_template_child (widget_class, InWindow, list_view_sw);
  gtk_widget_class_bind_template_child (widget_class, InWindow, issue_page);
  gtk_widget_class_bind_template_child (widget_class, InWindow, search_revealer);
  gtk_widget_class_bind_template_child (widget_class, InWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, InWindow, details_stack);
  gtk_widget_class_bind_template_child (widget_class, InWindow, search_entry);
  gtk_widget_class_bind_template_child (widget_class, InWindow, backend_revealer);
  gtk_widget_class_bind_template_child (widget_class, InWindow, backend_spinner);
  gtk_widget_class_bind_template_callback (widget_class, in_window_issue_activated);
  gtk_widget_class_bind_template_callback (widget_class, in_window_search_toggled);
  gtk_widget_class_bind_template_callback (widget_class, in_window_search_changed);
}

gchar *
get_created_at (GObject     *object,
                InIssue     *issue)
{
  if (issue)
    {
      GDateTime *dt;
      dt = in_issue_get_created_at (issue);
      
      return g_date_time_format (dt, "%x");
    }
  
  return g_strdup("");
}

static gint
sort_issues_by_date (gconstpointer issue_a,
                     gconstpointer issue_b,
                     gpointer      user_data)
{
  InIssue *a = (InIssue *) issue_a;
  InIssue *b = (InIssue *) issue_b;

  return g_date_time_compare (in_issue_get_created_at (b), in_issue_get_created_at (a));
}

static void
in_window_load (InWindow *self)
{
  InApplication *app = NULL;
  InDatabase *database = NULL;
  GPtrArray *issues;

  g_return_if_fail (IN_IS_WINDOW (self));

  app = IN_APPLICATION (g_application_get_default ());
  database = in_application_get_database (app);
  issues = in_database_get_all_issues (database);
  if (issues == NULL || issues->len == 0) return;
  for (guint i = 0; i < issues->len; i++)
    {
      InIssue *issue = g_ptr_array_index (issues, i);
      in_issue_list_model_add (self->model, issue);
    }

  gtk_stack_set_visible_child_name (self->stack, "main");
}

static void
backend_start (InIssueService *service,
               gpointer        user_data)
{
  InWindow *self = IN_WINDOW (user_data);

  gtk_revealer_set_reveal_child (self->backend_revealer, TRUE);
  gtk_spinner_start (self->backend_spinner);
}

static void
backend_stop (InIssueService *service,
              gpointer        user_data)
{
  InWindow *self = IN_WINDOW (user_data);

  gtk_revealer_set_reveal_child (self->backend_revealer, FALSE);
  gtk_spinner_stop (self->backend_spinner);
}

static void
in_window_init (InWindow *self)
{
  GtkFilterListModel *filter_model;
  GtkExpression *expression;
  GtkExpression *state_expression;
  GtkStringFilter *state_filter;
  GListModel *sort_model;
  GtkSorter *sorter;
  GtkSelectionModel *selection_model;
  GtkEveryFilter *filter;

  main_thread = g_thread_self ();

  gtk_widget_init_template (GTK_WIDGET (self));

  self->model = in_issue_list_model_new ();
  expression = gtk_property_expression_new (IN_TYPE_ISSUE, NULL, "title");
  state_expression = gtk_property_expression_new (IN_TYPE_ISSUE, NULL, "state");
  state_filter = gtk_string_filter_new (state_expression);
  gtk_string_filter_set_search (state_filter, "opened");
  filter = gtk_every_filter_new ();

  self->string_filter = GTK_FILTER (gtk_string_filter_new (expression));
  gtk_multi_filter_append (GTK_MULTI_FILTER (filter), GTK_FILTER (state_filter));
  gtk_multi_filter_append (GTK_MULTI_FILTER (filter), self->string_filter);
  filter_model = gtk_filter_list_model_new (G_LIST_MODEL (self->model), GTK_FILTER (filter));
  sorter = GTK_SORTER (gtk_custom_sorter_new (sort_issues_by_date, NULL, NULL));
  sort_model = G_LIST_MODEL (gtk_sort_list_model_new (G_LIST_MODEL (filter_model), sorter));

  selection_model = GTK_SELECTION_MODEL (gtk_single_selection_new (sort_model));
  gtk_list_view_set_model (self->list_view, selection_model);
  gtk_selection_model_select_item (selection_model, 10, TRUE);

  /* in_window_load (self); */

}
