/* is-application.c
 *
 * Copyright 2021 Günther Wagner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "in-application.h"
#include "in-window.h"
#include "in-issue-service.h"
#include "in-config.h"
#include <libpeas/peas.h>

struct _InApplication
{
  AdwApplication parent_instance;

  InDatabase *database;
  InIssueService *service;
};


G_DEFINE_TYPE (InApplication, in_application, ADW_TYPE_APPLICATION)

InApplication *
in_application_new (gchar *application_id,
                    GApplicationFlags  flags)
{
  return g_object_new (IN_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
in_application_finalize (GObject *object)
{
  InApplication *self = (InApplication *)object;

  g_clear_pointer (&self->database, g_object_unref);

  G_OBJECT_CLASS (in_application_parent_class)->finalize (object);
}

static void
load_css_provider (InApplication *self)
{
  g_autoptr(GtkCssProvider) provider = NULL;
  
  provider = gtk_css_provider_new ();
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_APPLICATION + 1);
  gtk_css_provider_load_from_resource (provider, "/de/gunibert/IssueNavigator/gui/Adwaita.css");
}

static void
in_application_activate (GApplication *app)
{
  InApplication *self;
  GtkWindow *window;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));
  
  self = IN_APPLICATION (app);
  
  load_css_provider (self);

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (IN_TYPE_WINDOW,
                           "application", app,
                           "default-width", 600,
                           "default-height", 300,
                           NULL);

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);
}

static void
in_application_load_plugins (InApplication *self)
{
  PeasEngine *engine;
  const GList *plugins;

  engine = peas_engine_get_default ();
  peas_engine_prepend_search_path (engine, "resource:///plugins", "resource:///plugins");
  peas_engine_prepend_search_path (engine,
                                   PACKAGE_LIBDIR"/issue-navigator/plugins",
                                   PACKAGE_DATADIR"/issue-navigator/plugins");
  g_irepository_prepend_search_path (PACKAGE_LIBDIR"/issue-navigator/girepository-1.0");
  g_irepository_prepend_search_path (PACKAGE_DATADIR"/issue-navigator/gir-1.0");
  g_irepository_require (NULL, "Gdk", "4.0", 0, NULL);
  peas_engine_enable_loader (engine, "python3");

  plugins = peas_engine_get_plugin_list (engine);

  for (const GList *iter = plugins; iter; iter = iter->next)
    {
      PeasPluginInfo *plugin_info = iter->data;

      if (!peas_plugin_info_is_loaded (plugin_info))
        peas_engine_load_plugin (engine, plugin_info);
    }
}

static void
in_application_startup (GApplication *app)
{
  InApplication *self = IN_APPLICATION (app);
  G_APPLICATION_CLASS (in_application_parent_class)->startup (app);
  in_application_load_plugins (IN_APPLICATION (app));
  in_issue_service_trigger (self->service);
}

static void
in_application_class_init (InApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = in_application_finalize;

  /*
   * We connect to the activate callback to create a window when the application
   * has been launched. Additionally, this callback notifies us when the user
   * tries to launch a "second instance" of the application. When they try
   * to do that, we'll just present any existing window.
   */
  app_class->activate = in_application_activate;
  app_class->startup = in_application_startup;
}

static void
in_application_show_about (GSimpleAction *action,
                           GVariant      *parameter,
                           gpointer       user_data)
{
  InApplication *self = IN_APPLICATION (user_data);
  GtkWindow *window = NULL;
  const gchar *authors[] = {"Günther Wagner", NULL};

  g_return_if_fail (IN_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  gtk_show_about_dialog (window,
                         "program-name", "is",
                         "authors", authors,
                         "version", "0.1.0",
                         NULL);
}


static void
in_application_init (InApplication *self)
{
  GSimpleAction *quit_action;
  GSimpleAction *about_action;
  const char *accels[] = {"<primary>q", NULL};

  quit_action = g_simple_action_new ("quit", NULL);
  g_signal_connect_swapped (quit_action, "activate", G_CALLBACK (g_application_quit), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (quit_action));

  about_action = g_simple_action_new ("about", NULL);
  g_signal_connect (about_action, "activate", G_CALLBACK (in_application_show_about), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (about_action));

  gtk_application_set_accels_for_action (GTK_APPLICATION (self), "app.quit", accels);

  self->database = in_database_new ();
  self->service = in_issue_service_get_default ();
}

/**
 * in_application_get_database:
 * @self: an #InApplication
 *
 * Returns the #InDatabase
 *
 * Returns: (transfer none): a #InDatabase
 */
InDatabase *
in_application_get_database (InApplication *self)
{
  g_return_val_if_fail (IN_IS_APPLICATION (self), NULL);

  return self->database;
}
