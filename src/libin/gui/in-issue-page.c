#include "in-issue-page.h"
#include "in-pango-parser.h"
#include "in-database.h"
#include "in-application.h"
#include <libpeas/peas.h>
#include <adwaita.h>
#include "in-issue-provider.h"

struct _InIssuePage
{
  GtkWidget parent_instance;
  
  AdwAvatar *avatar;
  GtkLabel *avatar_text;
  GtkLabel *title;
  AdwBin *description;
  GtkComboBoxText *state;
  GtkScrolledWindow *scrollview;
  GtkListBox *notes_view;

  InIssue *issue;
  InPangoParser *pango_parser;
};

G_DEFINE_TYPE (InIssuePage, in_issue_page, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  N_PROPS
};

InIssuePage *
in_issue_page_new (void)
{
  return g_object_new (IN_TYPE_ISSUE_PAGE, NULL);
}

static void
in_issue_page_finalize (GObject *object)
{
  InIssuePage *self = (InIssuePage *)object;

  G_OBJECT_CLASS (in_issue_page_parent_class)->finalize (object);
}

static void
in_issue_page_dispose (GObject *object)
{
  InIssuePage *self = (InIssuePage *)object;

  g_clear_pointer ((GtkWidget **)&self->scrollview, gtk_widget_unparent);

  G_OBJECT_CLASS (in_issue_page_parent_class)->dispose (object);
}

static void
in_issue_page_open_web_url (GtkButton *btn,
                            gpointer   user_data)
{
  InIssuePage *self = IN_ISSUE_PAGE (user_data);

  gtk_show_uri (NULL, in_issue_get_web_url (self->issue), GDK_CURRENT_TIME);
}

static void
in_issue_page_class_init (InIssuePageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = in_issue_page_finalize;
  object_class->dispose = in_issue_page_dispose;
  
  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/de/gunibert/IssueNavigator/gui/in-issue-page.ui");
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, avatar);
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, avatar_text);
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, title);
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, description);
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, state);
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, scrollview);
  gtk_widget_class_bind_template_child (widget_class, InIssuePage, notes_view);
  gtk_widget_class_bind_template_callback (widget_class, in_issue_page_open_web_url);
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static GtkWidget *
in_issue_page_bind_note (GObject  *item,
                         gpointer  user_data)
{
  InIssuePage *self = IN_ISSUE_PAGE (user_data);
  InNote *note = IN_NOTE (item);
  GtkWidget *avatar_lbl;
  GtkWidget *avatar;
  GtkWidget *lbl;

  InApplication *app = IN_APPLICATION (g_application_get_default ());
  InDatabase *db = in_application_get_database (app);

  InAuthor *author = in_database_get_author (db, IN_PERSONAL_RESOURCE (note));

  GtkStyleContext *style_context;
  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);

  GtkWidget *avatar_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  g_object_set (avatar_box, "width-request", 90, NULL);
  avatar_lbl = gtk_label_new (in_author_get_name (author));
  gtk_label_set_justify (GTK_LABEL (avatar_lbl), GTK_JUSTIFY_CENTER);
  gtk_label_set_max_width_chars (GTK_LABEL (avatar_lbl), 8);
  gtk_label_set_wrap (GTK_LABEL (avatar_lbl), TRUE);
  style_context = gtk_widget_get_style_context (avatar_lbl);
  gtk_style_context_add_class (style_context, "avatar-text");
  avatar = adw_avatar_new (48, in_author_get_name (author), TRUE);
  adw_avatar_set_custom_image (ADW_AVATAR (avatar), GDK_PAINTABLE (in_author_get_image (author)));
  gtk_widget_set_margin_top (avatar, 18);
  gtk_widget_set_valign (avatar, GTK_ALIGN_START);
  gtk_box_append (GTK_BOX (avatar_box), avatar);
  gtk_box_append (GTK_BOX (avatar_box), avatar_lbl);

  /* lbl = gtk_label_new (NULL); */
  gtk_box_append (GTK_BOX (box), avatar_box);
  /* gtk_box_append (GTK_BOX (box), lbl); */

  /* gtk_label_set_wrap (GTK_LABEL (lbl), TRUE); */
  /* gtk_label_set_xalign (GTK_LABEL (lbl), 0.0); */
  /* gtk_widget_set_hexpand (lbl, TRUE); */
  /* style_context = gtk_widget_get_style_context (lbl); */
  /* gtk_style_context_add_class (style_context, "issue-description"); */
  /* gtk_label_set_markup (GTK_LABEL (lbl), */
  gtk_box_append (GTK_BOX (box), in_pango_parser_parse (IN_PANGO_PARSER (self->pango_parser),
                                               (gchar *)in_note_get_body (note)));

  return box;
}

static void
in_issue_page_init (InIssuePage *self)
{
  PeasEngine *engine;
  const GList *plugins = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

  engine = peas_engine_get_default ();
  plugins = peas_engine_get_plugin_list (engine);

  for (; plugins; plugins = plugins->next)
    {
      PeasPluginInfo *plugin_info = plugins->data;
      if (peas_engine_provides_extension (engine, plugin_info, IN_TYPE_PANGO_PARSER))
        {
          PeasExtension *exten = peas_engine_create_extension (engine, plugin_info, IN_TYPE_PANGO_PARSER, NULL);
          self->pango_parser = IN_PANGO_PARSER (exten);
          break;
        }
    }
}

void
in_issue_page_set_issue (InIssuePage *self,
                         InIssue     *issue)
{
  GtkWidget *markup = NULL;
  InApplication *app;
  InDatabase *db;
  InAuthor *author;

  g_return_if_fail (IN_IS_ISSUE_PAGE (self));
  g_return_if_fail (IN_IS_ISSUE (issue));

  self->issue = issue;

  markup = in_pango_parser_parse (self->pango_parser, in_issue_get_description (issue));

  gtk_label_set_text (self->title, in_issue_get_title (issue));
  adw_bin_set_child (self->description, markup);
  gtk_combo_box_set_active_id (GTK_COMBO_BOX (self->state), in_issue_get_state (issue));

  app = IN_APPLICATION (g_application_get_default ());
  db = in_application_get_database (app);

  author = in_database_get_author (db, IN_PERSONAL_RESOURCE (issue));
  adw_avatar_set_text (self->avatar, in_author_get_name (author));

  adw_avatar_set_custom_image (self->avatar, GDK_PAINTABLE (in_author_get_image (author)));
  gtk_label_set_text (self->avatar_text, in_author_get_name (author));
  gtk_list_box_bind_model (self->notes_view, NULL, NULL, NULL, NULL);
}

void
in_issue_page_comments_fetched (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  InIssueProvider *provider = (InIssueProvider *)object;
  InIssuePage *self = (InIssuePage *)user_data;
  g_autoptr(GError) error = NULL;
  GPtrArray *comments;
  GListStore *store;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));

  if (!in_issue_provider_fetch_comments_finish (provider, result, &comments, &error))
    {
      g_warning ("%s", error->message);
      return;
    }

  store = g_list_store_new (IN_TYPE_NOTE);
  for (guint i = 0; i < comments->len; i++)
    {
      g_list_store_append (store, g_ptr_array_index (comments, i));
    }

  gtk_list_box_bind_model (self->notes_view,
                           G_LIST_MODEL (store),
                           (GtkListBoxCreateWidgetFunc) in_issue_page_bind_note,
                           self,
                           NULL);

}
