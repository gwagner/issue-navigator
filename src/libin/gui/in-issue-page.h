#pragma once

#include <gtk/gtk.h>
#include "in-issue.h"

G_BEGIN_DECLS

#define IN_TYPE_ISSUE_PAGE (in_issue_page_get_type())

G_DECLARE_FINAL_TYPE (InIssuePage, in_issue_page, IN, ISSUE_PAGE, GtkWidget)

InIssuePage *in_issue_page_new              (void);
void         in_issue_page_set_issue        (InIssuePage  *self,
                                             InIssue      *issue);
void         in_issue_page_comments_fetched (GObject      *object,
                                             GAsyncResult *result,
                                             gpointer      user_data);

G_END_DECLS
