/* in-note-data-mapper.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "in-data-mapper.h"
#include "in-note.h"
#include "in-issue.h"

G_BEGIN_DECLS

#define IN_TYPE_NOTE_DATA_MAPPER (in_note_data_mapper_get_type())

G_DECLARE_FINAL_TYPE (InNoteDataMapper, in_note_data_mapper, IN, NOTE_DATA_MAPPER, InDataMapper)

InNoteDataMapper *in_note_data_mapper_new           (InAdapter          *adapter);
void              in_note_data_mapper_insert        (InNoteDataMapper   *self,
                                                     InNote             *note);
InNote           *in_note_data_mapper_get           (InNoteDataMapper   *self,
                                                     const gchar        *id);
GPtrArray        *in_note_data_mapper_get_all       (InNoteDataMapper   *self);
GPtrArray        *in_note_data_mapper_get_for_issue (InNoteDataMapper   *self,
                                                     InPersonalResource *presource);

G_END_DECLS
