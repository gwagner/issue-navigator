/* in-data-mapper.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "in-adapter.h"

G_BEGIN_DECLS

#define IN_TYPE_DATA_MAPPER (in_data_mapper_get_type())

G_DECLARE_DERIVABLE_TYPE (InDataMapper, in_data_mapper, IN, DATA_MAPPER, GObject)

struct _InDataMapperClass
{
  GObjectClass parent_class;

  void         (*create_table)    (InDataMapper *self,
                                   InAdapter    *adapter);
  const gchar *(*get_table_name)  (InDataMapper *self);
  GObject     *(*create_object)   (InDataMapper *self);
  GType        (*get_object_type) (InDataMapper *self);
};

InDataMapper *in_data_mapper_new         (InAdapter     *adapter);
InAdapter    *in_data_mapper_get_adapter (InDataMapper  *self);
void          in_data_mapper_bind_params (InDataMapper  *self,
                                          gint           pos,
                                          sqlite3_stmt **stmt,
                                          GType          type,
                                          const GValue  *value);
void          in_data_mapper_insert      (InDataMapper  *self,
                                          GObject       *object);
void          in_data_mapper_update      (InDataMapper  *self,
                                          GObject       *object,
                                          GParamSpec    *spec_pkey);
GObject      *in_data_mapper_get         (InDataMapper  *self,
                                          const gchar   *id);
GPtrArray    *in_data_mapper_get_all     (InDataMapper  *self);
void          in_data_mapper_get_value   (GType          type,
                                          sqlite3_stmt  *stmt,
                                          gint           pos,
                                          GValue        *value);
G_END_DECLS
