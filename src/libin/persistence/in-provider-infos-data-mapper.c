/* in-provider-infos-data-mapper.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-provider-infos-data-mapper.h"

struct _InProviderInfosDataMapper
{
  InDataMapper parent_instance;
};

G_DEFINE_FINAL_TYPE (InProviderInfosDataMapper, in_provider_infos_data_mapper, IN_TYPE_DATA_MAPPER)

InProviderInfosDataMapper *
in_provider_infos_data_mapper_new (InAdapter *adapter)
{
  return g_object_new (IN_TYPE_PROVIDER_INFOS_DATA_MAPPER,
                       "adapter", adapter,
                       NULL);
}

static void
in_provider_infos_data_mapper_create_table (InDataMapper *data_mapper,
                                            InAdapter    *adapter)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS_DATA_MAPPER (data_mapper));
  g_return_if_fail (IN_IS_ADAPTER (adapter));

  in_adapter_execute (adapter, "CREATE TABLE IF NOT EXISTS provider_infos ("
      "  name TEXT,"
      "  url TEXT,"
      "  client_id TEXT,"
      "  client_secret TEXT,"
      "  access_token TEXT,"
      "  refresh_token TEXT,"
      "  expiration_date TEXT,"
      "  PRIMARY KEY (name, url)"
      ")");
}

static const gchar *
in_provider_infos_data_mapper_get_table_name (InDataMapper *data_mapper)
{
  return "provider_infos";
}

static GObject *
in_provider_infos_data_mapper_create_object (InDataMapper *data_mapper)
{
  return G_OBJECT (in_provider_infos_new ());
}

static GType
in_provider_infos_data_mapper_get_object_type (InDataMapper *data_mapper)
{
  return IN_TYPE_PROVIDER_INFOS;
}

static void
in_provider_infos_data_mapper_class_init (InProviderInfosDataMapperClass *klass)
{
  InDataMapperClass *data_mapper_class = IN_DATA_MAPPER_CLASS (klass);

  data_mapper_class->create_table = in_provider_infos_data_mapper_create_table;
  data_mapper_class->get_table_name = in_provider_infos_data_mapper_get_table_name;
  data_mapper_class->create_object = in_provider_infos_data_mapper_create_object;
  data_mapper_class->get_object_type = in_provider_infos_data_mapper_get_object_type;
}

static void
in_provider_infos_data_mapper_init (InProviderInfosDataMapper *self)
{
}

InProviderInfos *
in_provider_infos_data_mapper_get (InProviderInfosDataMapper *self,
                                   const gchar               *provider,
                                   const gchar               *baseurl)
{
  guint n_props;
  gint rc;
  GValue value = G_VALUE_INIT;
  GObject *object = NULL;

  g_return_val_if_fail (IN_IS_PROVIDER_INFOS_DATA_MAPPER (self), NULL);

  g_autoptr(GString) sql = g_string_new ("SELECT ");
  GTypeClass *klass = g_type_class_ref (IN_DATA_MAPPER_GET_CLASS (self)->get_object_type (self));
  GParamSpec **specs = g_object_class_list_properties (G_OBJECT_CLASS (klass), &n_props);
  g_type_class_unref (klass);

  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, ", ");
      else
        {
          g_string_append (sql, " FROM ");
          g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (self));
          g_string_append (sql, " WHERE name = ? AND url = ?");
        }
    }

  sqlite3_stmt *stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, provider);
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), 1, &stmt, G_TYPE_STRING, &value);
  g_value_unset (&value);

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, baseurl);
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), 2, &stmt, G_TYPE_STRING, &value);
  g_value_unset (&value);

  while ((rc = sqlite3_step (stmt)) != SQLITE_DONE)
    {
      object = IN_DATA_MAPPER_GET_CLASS (self)->create_object (self);
      g_object_set_data (object, "is-update", GINT_TO_POINTER (TRUE));
      gint count = sqlite3_column_count (stmt);
      for (gint i = 0; i < count; i++)
        {
          const gchar *column_name = sqlite3_column_name (stmt, i);
          for (guint j = 0; j < n_props; j++)
            {
              g_autoptr(GString) s = g_string_new (specs[j]->name);
              g_string_replace (s, "-", "_", -1);
              if (g_strcmp0 (s->str, column_name) == 0)
                {
                  GValue v = G_VALUE_INIT;
                  g_value_init (&v, specs[j]->value_type);
                  in_data_mapper_get_value (specs[j]->value_type, stmt, i, &v);

                  g_object_set_property (object, specs[j]->name, &v);
                  g_value_unset (&v);
                }
            }

        }
    }

  sqlite3_finalize (stmt);

  return IN_PROVIDER_INFOS (object);
}

void
in_provider_infos_data_mapper_insert (InProviderInfosDataMapper *self,
                                      InProviderInfos           *infos)
{
  g_return_if_fail (IN_IS_PROVIDER_INFOS_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_PROVIDER_INFOS (infos));

  in_data_mapper_insert (IN_DATA_MAPPER (self), G_OBJECT (infos));
}

void
in_provider_infos_data_mapper_update (InProviderInfosDataMapper *self,
                                      InProviderInfos           *infos)
{
  guint n_props;
  gint rc;
  GValue value = G_VALUE_INIT;

  g_return_if_fail (IN_IS_PROVIDER_INFOS_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_PROVIDER_INFOS (infos));

  g_autoptr(GString) sql = g_string_new ("UPDATE ");
  g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (self));
  g_string_append (sql, " SET ");

  GTypeClass *klass = g_type_class_ref (IN_DATA_MAPPER_GET_CLASS (self)->get_object_type (self));
  GParamSpec **specs = g_object_class_list_properties (G_OBJECT_CLASS (klass), &n_props);
  g_type_class_unref (klass);

  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, " = ?, ");
      else
        g_string_append (sql, " = ? WHERE ");
    }

  g_string_append (sql, "name = ? AND url = ?");

  sqlite3_stmt *stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  guint i;
  for (i = 0; i < n_props; i++)
    {
      GValue value = G_VALUE_INIT;
      g_value_init (&value, specs[i]->value_type);
      g_object_get_property (G_OBJECT (infos), specs[i]->name, &value);
      in_data_mapper_bind_params (IN_DATA_MAPPER (self), i + 1, &stmt, specs[i]->value_type, &value);
      g_value_unset (&value);
    }

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, in_provider_infos_get_name (infos));
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), i+1, &stmt, G_TYPE_STRING, &value);
  g_value_unset (&value);

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, in_provider_infos_get_url (infos));
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), i+2, &stmt, G_TYPE_STRING, &value);
  g_value_unset (&value);

  rc = sqlite3_step (stmt);
  if (rc != SQLITE_OK && rc != SQLITE_DONE)
    g_error("%s", sqlite3_errstr (rc));

  sqlite3_finalize (stmt);
}
