/* in-note-data-mapper.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-note-data-mapper.h"
#include "in-note.h"

struct _InNoteDataMapper
{
  InDataMapper parent_instance;
};

G_DEFINE_FINAL_TYPE (InNoteDataMapper, in_note_data_mapper, IN_TYPE_DATA_MAPPER)

InNoteDataMapper *
in_note_data_mapper_new (InAdapter *adapter)
{
  return g_object_new (IN_TYPE_NOTE_DATA_MAPPER,
                       "adapter", adapter,
                       NULL);
}

static const gchar *
in_note_data_mapper_get_table_name (InDataMapper *data_mapper)
{
  return "notes";
}

static GType
in_note_data_mapper_get_object_type (InDataMapper *data_mapper)
{
  return IN_TYPE_NOTE;
}

static GObject *
in_note_data_mapper_create_object (InDataMapper *data_mapper)
{
  return G_OBJECT (in_note_new ());
}

static void
in_note_data_mapper_create_table (InDataMapper *data_mapper,
                                  InAdapter    *adapter)
{
  g_return_if_fail (IN_IS_NOTE_DATA_MAPPER (data_mapper));
  g_return_if_fail (IN_IS_ADAPTER (adapter));

  in_adapter_execute (adapter, "CREATE TABLE IF NOT EXISTS notes ("
        "  id TEXT PRIMARY KEY,"
        "  author_id INTEGER,"
        "  parent_id INTEGER,"
        "  body TEXT,"
        "  created_at TEXT"
        ")");

}

static void
in_note_data_mapper_class_init (InNoteDataMapperClass *klass)
{
  InDataMapperClass *data_mapper_class = IN_DATA_MAPPER_CLASS (klass);

  data_mapper_class->get_table_name = in_note_data_mapper_get_table_name;
  data_mapper_class->get_object_type = in_note_data_mapper_get_object_type;
  data_mapper_class->create_object = in_note_data_mapper_create_object;
  data_mapper_class->create_table = in_note_data_mapper_create_table;
}

static void
in_note_data_mapper_init (InNoteDataMapper *self)
{
}

void
in_note_data_mapper_insert (InNoteDataMapper *self,
                            InNote           *note)
{
  g_return_if_fail (IN_IS_NOTE_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_NOTE (note));

  in_data_mapper_insert (IN_DATA_MAPPER (self), G_OBJECT (note));
}

/**
 * in_note_data_mapper_get:
 *
 * Returns: (transfer full): an #InNote
 */
InNote *
in_note_data_mapper_get (InNoteDataMapper *self,
                         const gchar      *id)
{
  g_return_val_if_fail (IN_IS_NOTE_DATA_MAPPER (self), NULL);

  return IN_NOTE (in_data_mapper_get (IN_DATA_MAPPER (self), id));
}

/**
 * in_note_data_mapper_get_all:
 *
 * Returns: (element-type InNote) (transfer full): an array of #InNote elements
 */
GPtrArray *in_note_data_mapper_get_all (InNoteDataMapper *self)
{
  g_return_val_if_fail (IN_IS_NOTE_DATA_MAPPER (self), NULL);

  return in_data_mapper_get_all (IN_DATA_MAPPER (self));
}

/**
 * in_note_data_mapper_get_for_issue:
 *
 * Returns: (element-type InNote) (transfer full): an array of #InNote elements
 */
GPtrArray *
in_note_data_mapper_get_for_issue (InNoteDataMapper   *self,
                                   InPersonalResource *presource)
{
  GString *sql;
  GTypeClass *klass;
  guint n_props;
  sqlite3_stmt *stmt;
  gint rc;
  GObject *object = NULL;
  GPtrArray *ret;
  GParamSpec **specs;
  GValue value = G_VALUE_INIT;

  g_return_val_if_fail (IN_IS_DATA_MAPPER (self), NULL);

  ret = g_ptr_array_new ();

  //"SELECT id, title, description, state, web_url, created_at FROM issues";
  sql = g_string_new ("SELECT ");

  klass = g_type_class_ref (IN_DATA_MAPPER_GET_CLASS (self)->get_object_type (IN_DATA_MAPPER (self)));

  specs = g_object_class_list_properties (G_OBJECT_CLASS (klass), &n_props);
  g_type_class_unref (klass);
  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, ", ");
      else
        {
          g_string_append (sql, " FROM ");
          g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (IN_DATA_MAPPER (self)));
          g_string_append (sql, " WHERE parent_id = ?");
        }
    }

  stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, in_personal_resource_get_id (presource));
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), 1, &stmt, G_TYPE_STRING, &value);
  g_value_unset (&value);

  g_debug ("%s", sqlite3_sql (stmt));

  while ((rc = sqlite3_step (stmt)) != SQLITE_DONE)
    {
      gint count;
      object = IN_DATA_MAPPER_GET_CLASS (self)->create_object (IN_DATA_MAPPER (self));
      g_object_set_data (object, "is-update", GINT_TO_POINTER (TRUE));
      count = sqlite3_column_count (stmt);
      for (gint i = 0; i < count; i++)
        {
          const gchar *column_name = sqlite3_column_name (stmt, i);
          for (guint j = 0; j < n_props; j++)
            {
              g_autoptr(GString) s = g_string_new (specs[j]->name);
              g_string_replace (s, "-", "_", -1);
              if (g_strcmp0 (s->str, column_name) == 0)
                {
                  GValue v = G_VALUE_INIT;
                  g_value_init (&v, specs[j]->value_type);
                  in_data_mapper_get_value (specs[j]->value_type, stmt, i, &v);

                  g_object_set_property (object, specs[j]->name, &v);
                  g_value_unset (&v);
                }
            }

        }
      g_ptr_array_add (ret, object);
    }

  return ret;

}
