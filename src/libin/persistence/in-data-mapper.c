/* in-data-mapper.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-data-mapper.h"
#include <gdk/gdk.h>

typedef struct
{
  InAdapter *adapter;
} InDataMapperPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (InDataMapper, in_data_mapper, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ADAPTER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * in_data_mapper_new:
 *
 * Create a new #InDataMapper.
 *
 * Returns: (transfer full): a newly created #InDataMapper
 */
InDataMapper *
in_data_mapper_new (InAdapter *adapter)
{
  return g_object_new (IN_TYPE_DATA_MAPPER,
                       "adapter", adapter,
                       NULL);
}

static void
in_data_mapper_constructed (GObject *object)
{
  InDataMapper *self = IN_DATA_MAPPER (object);
  InDataMapperPrivate *priv = in_data_mapper_get_instance_private (self);

  if (IN_DATA_MAPPER_GET_CLASS (self)->create_table)
    IN_DATA_MAPPER_GET_CLASS (self)->create_table (self, priv->adapter);
  else
    g_assert_not_reached ();
}

static void
in_data_mapper_finalize (GObject *object)
{
  InDataMapper *self = (InDataMapper *)object;
  InDataMapperPrivate *priv = in_data_mapper_get_instance_private (self);

  g_clear_object (&priv->adapter);

  G_OBJECT_CLASS (in_data_mapper_parent_class)->finalize (object);
}

static void
in_data_mapper_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  InDataMapper *self = IN_DATA_MAPPER (object);
  InDataMapperPrivate *priv = in_data_mapper_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ADAPTER:
      g_value_set_object (value, priv->adapter);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_data_mapper_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  InDataMapper *self = IN_DATA_MAPPER (object);
  InDataMapperPrivate *priv = in_data_mapper_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ADAPTER:
      g_clear_object (&priv->adapter);
      priv->adapter = g_value_dup_object (value);
      g_object_notify_by_pspec (object, properties[PROP_ADAPTER]);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_data_mapper_class_init (InDataMapperClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_data_mapper_finalize;
  object_class->get_property = in_data_mapper_get_property;
  object_class->set_property = in_data_mapper_set_property;
  object_class->constructed = in_data_mapper_constructed;

  properties [PROP_ADAPTER] =
    g_param_spec_object ("adapter",
                         "Adapter",
                         "Adapter",
                         IN_TYPE_ADAPTER,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_ADAPTER,
                                   properties [PROP_ADAPTER]);
}

static void
in_data_mapper_init (InDataMapper *self)
{
}

/**
 * in_data_mapper_get_adapter:
 * @self: a #InDataMapper
 *
 * Returns: (transfer none): a #InAdapter
 */
InAdapter *
in_data_mapper_get_adapter (InDataMapper *self)
{
  InDataMapperPrivate *priv = in_data_mapper_get_instance_private (self);

  g_return_val_if_fail (IN_IS_DATA_MAPPER (self), NULL);

  return priv->adapter;
}

GParamSpec **
_get_params (GObject *object, guint *n_props)
{
  GType type;
  GTypeClass *klass;
  GParamSpec **specs;

  type = G_OBJECT_TYPE (object);
  klass = g_type_class_ref (type);

  specs = g_object_class_list_properties (G_OBJECT_CLASS (klass), n_props);

  g_type_class_unref (klass);
  return specs;
}

/**
 * in_data_mapper_bind_params: (skip)
 */
void
in_data_mapper_bind_params (InDataMapper  *self,
                            gint           pos,
                            sqlite3_stmt **stmt,
                            GType          type,
                            const GValue  *value)
{
  switch (type)
    {
    case G_TYPE_INT:
      sqlite3_bind_int (*stmt, pos, g_value_get_int (value));
      break;
    case G_TYPE_STRING:
      sqlite3_bind_text (*stmt, pos, g_value_dup_string (value), -1, g_free);
      break;
    default:
      if (type == G_TYPE_DATE_TIME)
        {
          GDateTime *dt = g_value_get_boxed (value);
          gchar *iso8601;
          if (dt)
            {
              iso8601 = g_date_time_format_iso8601 (dt);
              sqlite3_bind_text (*stmt, pos, iso8601, -1, g_free);
            }
        }
      else if (type == GDK_TYPE_TEXTURE)
        {
          GdkTexture *texture = g_value_get_object (value);
          if (texture)
            {
              GBytes *bytes = gdk_texture_save_to_png_bytes (texture);
              sqlite3_bind_blob (*stmt,
                                 pos,
                                 g_bytes_get_data (bytes, NULL),
                                 g_bytes_get_size (bytes),
                                 NULL);

            }
          else
              sqlite3_bind_blob (*stmt,
                                 pos,
                                 NULL,
                                 0,
                                 NULL);
        }
      break;
    }
}

void
in_data_mapper_insert (InDataMapper *self,
                       GObject      *object)
{
  sqlite3_stmt *stmt;
  gint rc;
  GType type;
  GTypeClass *klass;
  guint n_props;
  GString *sql;
  GParamSpec **specs;

  g_return_if_fail (IN_IS_DATA_MAPPER (self));
  g_return_if_fail (G_IS_OBJECT (object));

  sql = g_string_new ("REPLACE into ");
  g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (self));
  g_string_append (sql, " (");

  specs = _get_params (object, &n_props);

  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, ", ");
      else
        g_string_append (sql, ") VALUES (");
    }

  for (guint i = 0; i < n_props; i++)
    {
      if (i != n_props - 1)
        g_string_append (sql, "?, ");
      else
        g_string_append (sql, "?);");
    }

  stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  for (guint i = 0; i < n_props; i++)
    {
      GValue value = G_VALUE_INIT;
      g_value_init (&value, specs[i]->value_type);
      g_object_get_property (object, specs[i]->name, &value);
      in_data_mapper_bind_params (IN_DATA_MAPPER (self), i + 1, &stmt, specs[i]->value_type, &value);
      g_value_unset (&value);
    }

  g_debug ("%s", sqlite3_sql (stmt));
  g_string_free (sql, TRUE);

  rc = sqlite3_step (stmt);
  if (rc != SQLITE_OK && rc != SQLITE_DONE)
    g_error("%s", sqlite3_errstr (rc));
  sqlite3_finalize (stmt);
}

void
in_data_mapper_update (InDataMapper *self,
                       GObject      *object,
                       GParamSpec   *spec_pkey)
{
  GString *sql;
  gint rc;
  GParamSpec **specs;
  guint n_props;
  sqlite3_stmt *stmt;
  GValue value = G_VALUE_INIT;

  g_return_if_fail (IN_IS_DATA_MAPPER (self));
  g_return_if_fail (G_IS_OBJECT (object));

  sql = g_string_new ("UPDATE ");
  g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (self));
  g_string_append (sql, " SET ");

  specs = _get_params (object, &n_props);

  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, " = ?, ");
      else
        g_string_append (sql, " = ? WHERE ");
    }

  g_string_append (sql, g_param_spec_get_name (spec_pkey));
  g_string_append (sql, " = ?");

  stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  for (guint i = 0; i < n_props; i++)
    {
      GValue value = G_VALUE_INIT;
      g_value_init (&value, specs[i]->value_type);
      g_object_get_property (object, specs[i]->name, &value);
      in_data_mapper_bind_params (IN_DATA_MAPPER (self), i + 1, &stmt, specs[i]->value_type, &value);
      g_value_unset (&value);
    }

  g_value_init (&value, spec_pkey->value_type);
  g_object_get_property (object, spec_pkey->name, &value);
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), n_props, &stmt, spec_pkey->value_type, &value);
  g_value_unset (&value);

  g_debug ("%s", sqlite3_sql (stmt));
  g_string_free (sql, TRUE);

  rc = sqlite3_step (stmt);
  if (rc != SQLITE_OK && rc != SQLITE_DONE)
    g_error("%s", sqlite3_errstr (rc));

  sqlite3_finalize (stmt);
}

/**
 * in_data_mapper_get_value: (skip)
 */
void
in_data_mapper_get_value (GType         type,
                          sqlite3_stmt *stmt,
                          gint          pos,
                          GValue       *value)
{
  switch (type)
    {
    case G_TYPE_INT:
      g_value_set_int (value, sqlite3_column_int (stmt, pos));
      break;
    case G_TYPE_STRING:
      g_value_set_string (value, (gchar *)sqlite3_column_text (stmt, pos));
      break;
    default:
      if (type == G_TYPE_DATE_TIME)
        {
          GDateTime *dt = g_date_time_new_from_iso8601 ((gchar *)sqlite3_column_text (stmt, pos), NULL);
          g_value_take_boxed (value, dt);
        }
      else if (type == GDK_TYPE_TEXTURE)
        {
          gconstpointer data = sqlite3_column_blob (stmt, pos);
          gsize size = sqlite3_column_bytes (stmt, pos);
          gpointer data_copy = g_memdup2 (data, size);
          GBytes *bytes = g_bytes_new_take (data_copy, size);
          GdkTexture *texture = gdk_texture_new_from_bytes (bytes, NULL);
          g_value_take_object (value, texture);
        }
      else
        g_assert_not_reached ();
    }
}

/**
 * in_data_mapper_get_all:
 *
 * Returns: (element-type GObject) (transfer full): an array of #GObject elements
 */
GPtrArray *
in_data_mapper_get_all (InDataMapper *self)
{
  g_autoptr(GString) sql;
  GTypeClass *klass;
  guint n_props;
  sqlite3_stmt *stmt;
  gint rc;
  GObject *object = NULL;
  GPtrArray *ret;
  GParamSpec **specs;

  g_return_val_if_fail (IN_IS_DATA_MAPPER (self), NULL);

  ret = g_ptr_array_new ();

  //"SELECT id, title, description, state, web_url, created_at FROM issues";
  sql = g_string_new ("SELECT ");

  klass = g_type_class_ref (IN_DATA_MAPPER_GET_CLASS (self)->get_object_type (self));

  specs = g_object_class_list_properties (G_OBJECT_CLASS (klass), &n_props);
  g_type_class_unref (klass);
  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, ", ");
      else
        {
          g_string_append (sql, " FROM ");
          g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (self));
        }
    }

  stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  g_debug ("%s", sqlite3_sql (stmt));

  while ((rc = sqlite3_step (stmt)) != SQLITE_DONE)
    {
      gint count;
      object = IN_DATA_MAPPER_GET_CLASS (self)->create_object (self);
      count = sqlite3_column_count (stmt);
      g_object_set_data (object, "is-update", GINT_TO_POINTER (TRUE));
      for (gint i = 0; i < count; i++)
        {
          const gchar *column_name = sqlite3_column_name (stmt, i);
          for (guint j = 0; j < n_props; j++)
            {
              g_autoptr(GString) s = g_string_new (specs[j]->name);
              g_string_replace (s, "-", "_", -1);
              if (g_strcmp0 (s->str, column_name) == 0)
                {
                  GValue v = G_VALUE_INIT;
                  g_value_init (&v, specs[j]->value_type);
                  in_data_mapper_get_value (specs[j]->value_type, stmt, i, &v);

                  g_object_set_property (object, specs[j]->name, &v);
                  g_value_unset (&v);
                }
            }

        }
      g_ptr_array_add (ret, object);
    }

  sqlite3_finalize (stmt);

  return ret;
}

/**
 * in_data_mapper_get:
 *
 * Returns: (transfer full): a #GObject
 */
GObject *
in_data_mapper_get (InDataMapper *self,
                    const gchar  *id)
{
  g_autoptr(GString) sql;
  GTypeClass *klass;
  guint n_props;
  sqlite3_stmt *stmt;
  gint rc;
  GObject *object = NULL;
  GParamSpec **specs;
  GValue value = G_VALUE_INIT;

  g_return_val_if_fail (IN_IS_DATA_MAPPER (self), NULL);

  //"SELECT id, title, description, state, web_url, created_at FROM issues WHERE id = ?";
  sql = g_string_new ("SELECT ");

  klass = g_type_class_ref (IN_DATA_MAPPER_GET_CLASS (self)->get_object_type (self));

  specs = g_object_class_list_properties (G_OBJECT_CLASS (klass), &n_props);
  g_type_class_unref (klass);
  for (guint i = 0; i < n_props; i++)
    {
      GParamSpec *spec = specs[i];
      g_string_append (sql, g_param_spec_get_name (spec));
      g_string_replace (sql, "-", "_", 0);
      if (i != n_props - 1)
        g_string_append (sql, ", ");
      else
        {
          g_string_append (sql, " FROM ");
          g_string_append (sql, IN_DATA_MAPPER_GET_CLASS (self)->get_table_name (self));
          g_string_append (sql, " WHERE id = ?");
        }
    }

  stmt = in_adapter_prepare (in_data_mapper_get_adapter (IN_DATA_MAPPER (self)), sql->str);

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, id);
  in_data_mapper_bind_params (IN_DATA_MAPPER (self), 1, &stmt, G_TYPE_STRING, &value);
  g_value_unset (&value);

  g_debug ("%s", sqlite3_sql (stmt));

  while ((rc = sqlite3_step (stmt)) != SQLITE_DONE)
    {
      gint count;
      object = IN_DATA_MAPPER_GET_CLASS (self)->create_object (self);
      g_object_set_data (object, "is-update", GINT_TO_POINTER (TRUE));
      count = sqlite3_column_count (stmt);
      for (gint i = 0; i < count; i++)
        {
          const gchar *column_name = sqlite3_column_name (stmt, i);
          for (guint j = 0; j < n_props; j++)
            {
              g_autoptr(GString) s = g_string_new (specs[j]->name);
              g_string_replace (s, "-", "_", -1);
              if (g_strcmp0 (s->str, column_name) == 0)
                {
                  GValue v = G_VALUE_INIT;
                  g_value_init (&v, specs[j]->value_type);
                  in_data_mapper_get_value (specs[j]->value_type, stmt, i, &v);

                  g_object_set_property (object, specs[j]->name, &v);
                  g_value_unset (&v);
                }
            }

        }
    }

  sqlite3_finalize (stmt);

  return object;
}
