/* in-author-data-mapper.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-author-data-mapper.h"

struct _InAuthorDataMapper
{
  InDataMapper parent_instance;
};

G_DEFINE_FINAL_TYPE (InAuthorDataMapper, in_author_data_mapper, IN_TYPE_DATA_MAPPER)

InAuthorDataMapper *
in_author_data_mapper_new (InAdapter *adapter)
{
  return g_object_new (IN_TYPE_AUTHOR_DATA_MAPPER,
                       "adapter", adapter,
                       NULL);
}

static void
in_author_data_mapper_create_table (InDataMapper *self,
                                    InAdapter    *adapter)
{
  g_return_if_fail (IN_IS_AUTHOR_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_ADAPTER (adapter));

  in_adapter_execute (adapter, "CREATE TABLE IF NOT EXISTS authors ("
        "  id TEXT PRIMARY KEY,"
        "  name TEXT,"
        "  image BLOB"
        ")");
}

static const gchar *
in_author_data_mapper_get_table_name (InDataMapper *data_mapper)
{
  return "authors";
}

static GObject *
in_author_data_mapper_create_object (InDataMapper *data_mapper)
{
  return G_OBJECT (in_author_new ());
}

static GType
in_author_data_mapper_get_object_type (InDataMapper *data_mapper)
{
  return IN_TYPE_AUTHOR;
}

static void
in_author_data_mapper_class_init (InAuthorDataMapperClass *klass)
{
  InDataMapperClass *data_mapper_class = IN_DATA_MAPPER_CLASS (klass);

  data_mapper_class->create_table = in_author_data_mapper_create_table;
  data_mapper_class->get_table_name = in_author_data_mapper_get_table_name;
  data_mapper_class->create_object = in_author_data_mapper_create_object;
  data_mapper_class->get_object_type = in_author_data_mapper_get_object_type;
}

static void
in_author_data_mapper_init (InAuthorDataMapper *self)
{
}

void
in_author_data_mapper_insert (InAuthorDataMapper *self,
                              InAuthor           *author)
{
  g_return_if_fail (IN_IS_AUTHOR_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_AUTHOR (author));

  in_data_mapper_insert (IN_DATA_MAPPER (self), G_OBJECT (author));
}

/**
 * in_author_data_mapper_get:
 *
 * Returns: (transfer full): an #InAuthor
 */
InAuthor *
in_author_data_mapper_get (InAuthorDataMapper *self,
                           const gchar        *id)
{
  g_return_val_if_fail (IN_IS_AUTHOR_DATA_MAPPER (self), NULL);

  return IN_AUTHOR (in_data_mapper_get (IN_DATA_MAPPER (self), id));
}
