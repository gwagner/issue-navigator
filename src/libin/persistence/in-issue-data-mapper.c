/* in-issue-data-mapper.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-issue-data-mapper.h"
#include <sqlite3.h>

struct _InIssueDataMapper
{
  InDataMapper parent_instance;
};

G_DEFINE_FINAL_TYPE (InIssueDataMapper, in_issue_data_mapper, IN_TYPE_DATA_MAPPER)

InIssueDataMapper *
in_issue_data_mapper_new (InAdapter *adapter)
{
  return g_object_new (IN_TYPE_ISSUE_DATA_MAPPER,
                       "adapter", adapter,
                       NULL);
}

static void
in_issue_data_mapper_create_table (InDataMapper *self,
                                   InAdapter    *adapter)
{
  g_return_if_fail (IN_IS_ISSUE_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_ADAPTER (adapter));

  in_adapter_execute (adapter, "CREATE TABLE IF NOT EXISTS issues ("
        "  id TEXT PRIMARY KEY,"
        "  author_id TEXT,"
        "  title TEXT,"
        "  description TEXT,"
        "  state TEXT,"
        "  web_url TEXT,"
        "  created_at TEXT"
        ")");
}

static const gchar *
in_issue_data_mapper_get_table_name (InDataMapper *data_mapper)
{
  g_return_val_if_fail (IN_IS_ISSUE_DATA_MAPPER (data_mapper), NULL);

  return "issues";
}

static GObject *
in_issue_data_mapper_create_object (InDataMapper *data_mapper)
{
  g_return_val_if_fail (IN_IS_ISSUE_DATA_MAPPER (data_mapper), NULL);

  return G_OBJECT (in_issue_new ());
}

static GType
in_issue_data_mapper_get_object_type (InDataMapper *data_mapper)
{
  g_return_val_if_fail (IN_IS_ISSUE_DATA_MAPPER (data_mapper), 0);

  return IN_TYPE_ISSUE;
}

static void
in_issue_data_mapper_class_init (InIssueDataMapperClass *klass)
{
  InDataMapperClass *data_mapper_class = IN_DATA_MAPPER_CLASS (klass);

  data_mapper_class->create_table = in_issue_data_mapper_create_table;
  data_mapper_class->get_table_name = in_issue_data_mapper_get_table_name;
  data_mapper_class->create_object = in_issue_data_mapper_create_object;
  data_mapper_class->get_object_type = in_issue_data_mapper_get_object_type;
}

static void
in_issue_data_mapper_init (InIssueDataMapper *self)
{
}

void
in_issue_data_mapper_insert (InIssueDataMapper *self,
                             InIssue           *issue)
{
  g_return_if_fail (IN_IS_ISSUE_DATA_MAPPER (self));
  g_return_if_fail (IN_IS_ISSUE (issue));

  in_data_mapper_insert (IN_DATA_MAPPER (self), G_OBJECT (issue));
}

/**
 * in_issue_data_mapper_get:
 *
 * Returns: (transfer full): a #InIssue from the database
 */
InIssue *
in_issue_data_mapper_get (InIssueDataMapper *self,
                          const gchar       *id)
{
  return IN_ISSUE (in_data_mapper_get (IN_DATA_MAPPER (self), id));
}

/**
 * in_issue_data_mapper_get_all:
 *
 * Returns: (element-type InIssue) (transfer full): an array of #InIssue elements
 */
GPtrArray *
in_issue_data_mapper_get_all (InIssueDataMapper *self)
{
  return in_data_mapper_get_all (IN_DATA_MAPPER (self));
}

void
in_issue_data_mapper_update (InIssueDataMapper *self,
                             InIssue           *issue)
{
  GParamSpec *spec = g_object_class_find_property (G_OBJECT_GET_CLASS (issue), "id");
  in_data_mapper_update (IN_DATA_MAPPER (self), G_OBJECT (issue), spec);
}
