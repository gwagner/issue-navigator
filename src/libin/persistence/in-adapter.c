/* in-adapter.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-adapter.h"

struct _InAdapter
{
  GObject parent_instance;
  sqlite3 *db;
};

G_DEFINE_FINAL_TYPE (InAdapter, in_adapter, G_TYPE_OBJECT)

InAdapter *
in_adapter_new (void)
{
  return g_object_new (IN_TYPE_ADAPTER, NULL);
}

static void
in_adapter_finalize (GObject *object)
{
  InAdapter *self = (InAdapter *)object;

  g_clear_pointer (&self->db, sqlite3_close_v2);

  G_OBJECT_CLASS (in_adapter_parent_class)->finalize (object);
}

static void
in_adapter_class_init (InAdapterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_adapter_finalize;
}

static void
in_adapter_init (InAdapter *self)
{
}

void
in_adapter_open (InAdapter   *self,
                 const gchar *filename)
{
  g_return_if_fail (IN_IS_ADAPTER (self));

  sqlite3_open_v2 (filename,
                   &self->db,
                   SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,
                   NULL);
}

/**
 * in_adapter_prepare: (skip)
 *
 * Returns: (skip)
 */
sqlite3_stmt *
in_adapter_prepare (InAdapter *self,
                    gchar     *sql)
{
  sqlite3_stmt *stmt;
  gint rc;
  g_return_val_if_fail (IN_IS_ADAPTER (self), NULL);

  rc = sqlite3_prepare_v2 (self->db, sql, -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));

  return stmt;
}

/**
 * in_adapter_execute: (skip)
 */
void
in_adapter_execute (InAdapter   *self,
                    const gchar *sql)
{
  sqlite3_stmt *stmt;
  gint rc;

  g_return_if_fail (IN_IS_ADAPTER (self));

  rc = sqlite3_prepare_v2 (self->db, sql, -1, &stmt, NULL);
  if (rc != SQLITE_OK)
    g_error ("%s", sqlite3_errmsg (self->db));
  sqlite3_step (stmt);
  sqlite3_finalize (stmt);
}
