/* in-database.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "in-core.h"

G_BEGIN_DECLS

#define IN_TYPE_DATABASE (in_database_get_type())

G_DECLARE_FINAL_TYPE (InDatabase, in_database, IN, DATABASE, GObject)

InDatabase      *in_database_new                (void);
void             in_database_setup              (InDatabase         *self);
void             in_database_save_resource      (InDatabase         *self,
                                                 GObject            *resource);
void             in_database_save_resources     (InDatabase         *self,
                                                 GList              *resources);
InIssue         *in_database_get_issue          (InDatabase         *self,
                                                 const gchar        *id);
InNote          *in_database_get_note           (InDatabase         *self,
                                                 const gchar        *id);
GPtrArray       *in_database_get_all_issues     (InDatabase         *self);
GPtrArray       *in_database_get_notes          (InDatabase         *self,
                                                 InIssue            *issue);
InAuthor        *in_database_get_author         (InDatabase         *self,
                                                 InPersonalResource *personal_resource);
InAuthor        *in_database_get_author_by_id   (InDatabase         *self,
                                                 const gchar        *id);
void             in_database_save_etag          (InDatabase         *self,
                                                 const gchar        *function,
                                                 const gchar        *etag);
const gchar     *in_database_get_etag           (InDatabase         *self,
                                                 const gchar        *function);
InProviderInfos *in_database_get_provider_infos (InDatabase         *self,
                                                 const gchar        *provider,
                                                 const gchar        *baseurl);

G_END_DECLS
