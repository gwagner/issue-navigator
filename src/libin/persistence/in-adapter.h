/* in-adapter.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <sqlite3.h>

G_BEGIN_DECLS

#define IN_TYPE_ADAPTER (in_adapter_get_type())

G_DECLARE_FINAL_TYPE (InAdapter, in_adapter, IN, ADAPTER, GObject)

InAdapter    *in_adapter_new     (void);
void          in_adapter_open    (InAdapter   *self,
                                  const gchar *filename);
sqlite3_stmt *in_adapter_prepare (InAdapter   *self,
                                  gchar       *sql);
void          in_adapter_execute (InAdapter   *self,
                                  const gchar *sql);
G_END_DECLS
