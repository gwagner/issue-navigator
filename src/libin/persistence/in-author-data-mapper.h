/* in-author-data-mapper.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "in-author.h"
#include "in-data-mapper.h"

G_BEGIN_DECLS

#define IN_TYPE_AUTHOR_DATA_MAPPER (in_author_data_mapper_get_type())

G_DECLARE_FINAL_TYPE (InAuthorDataMapper, in_author_data_mapper, IN, AUTHOR_DATA_MAPPER, InDataMapper)

InAuthorDataMapper *in_author_data_mapper_new    (InAdapter          *adapter);
void                in_author_data_mapper_insert (InAuthorDataMapper *self,
                                                  InAuthor           *author);
InAuthor           *in_author_data_mapper_get    (InAuthorDataMapper *self,
                                                  const gchar        *id);
G_END_DECLS
