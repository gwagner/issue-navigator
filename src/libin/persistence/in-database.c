/* in-database.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-database.h"
#include "in-config.h"
#include "sqlite3.h"
#include "in-adapter.h"
#include "in-issue-data-mapper.h"
#include "in-author-data-mapper.h"
#include "in-note-data-mapper.h"
#include "in-provider-infos-data-mapper.h"

struct _InDatabase
{
  GObject parent_instance;

  InAdapter *adapter;
  InIssueDataMapper *issue_mapper;
  InAuthorDataMapper *author_mapper;
  InNoteDataMapper *note_mapper;
  InProviderInfosDataMapper *provider_infos_mapper;
};

G_DEFINE_TYPE (InDatabase, in_database, G_TYPE_OBJECT)

InDatabase *
in_database_new (void)
{
  return g_object_new (IN_TYPE_DATABASE, NULL);
}

static void
in_database_finalize (GObject *object)
{
  InDatabase *self = (InDatabase *)object;

  G_OBJECT_CLASS (in_database_parent_class)->finalize (object);
}

static void
in_database_class_init (InDatabaseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_database_finalize;
}

static gchar *
get_database_file (void)
{
  g_autofree gchar *database_file = NULL;
  g_autofree gchar *app_data_dir = NULL;
  const gchar *data_dir;

  data_dir = g_get_user_data_dir ();
  database_file = g_build_filename (data_dir, "issue-navigator", "issues.db", NULL);
  app_data_dir = g_path_get_dirname (database_file);
  g_mkdir_with_parents (app_data_dir, 0750);

  return g_steal_pointer (&database_file);
}

static void
in_database_init (InDatabase *self)
{
  g_autofree gchar *database_file = NULL;

  database_file = get_database_file ();

  self->adapter = in_adapter_new ();
  in_adapter_open (self->adapter, database_file);
  self->issue_mapper = in_issue_data_mapper_new (self->adapter);
  self->author_mapper = in_author_data_mapper_new (self->adapter);
  self->note_mapper = in_note_data_mapper_new (self->adapter);
  self->provider_infos_mapper = in_provider_infos_data_mapper_new (self->adapter);

  in_adapter_execute (self->adapter, "CREATE TABLE IF NOT EXISTS etags ("
        "  function TEXT PRIMARY KEY,"
        "  etag TEXT"
        ");");
}

/**
 * in_database_save_resource:
 * @self: a #InDatabase
 * @resource: a #GomResource
 */
void
in_database_save_resource (InDatabase  *self,
                           GObject *resource)
{
  GType type;

  g_return_if_fail (IN_IS_DATABASE (self));
  g_return_if_fail (G_IS_OBJECT (resource));

  type = G_OBJECT_TYPE (resource);

  if (type == IN_TYPE_ISSUE)
    {
      gpointer is_update = g_object_get_data (resource, "is-update");
      if (is_update == NULL || !GPOINTER_TO_INT (is_update))
        in_issue_data_mapper_insert (self->issue_mapper, IN_ISSUE (resource));
      else
        in_issue_data_mapper_update (self->issue_mapper, IN_ISSUE (resource));
    }
  else if (type == IN_TYPE_AUTHOR)
    {
      gpointer is_update = g_object_get_data (resource, "is-update");
      if (is_update == NULL || !GPOINTER_TO_INT (is_update))
        in_author_data_mapper_insert (self->author_mapper, IN_AUTHOR (resource));
    }
  else if (type == IN_TYPE_NOTE)
    {
      gpointer is_update = g_object_get_data (resource, "is-update");
      if (is_update == NULL || !GPOINTER_TO_INT (is_update))
        in_note_data_mapper_insert (self->note_mapper, IN_NOTE (resource));
    }
  else if (type == IN_TYPE_PROVIDER_INFOS)
    {
      gpointer is_update = g_object_get_data (resource, "is-update");
      if (is_update == NULL || !GPOINTER_TO_INT (is_update))
        in_provider_infos_data_mapper_insert (self->provider_infos_mapper, IN_PROVIDER_INFOS (resource));
      else
        in_provider_infos_data_mapper_update (self->provider_infos_mapper, IN_PROVIDER_INFOS (resource));
    }
}

/**
 * in_database_save_resources:
 * @self: a #InDatabase
 * @resources: (element-type GObject): a #GList with all resources to save
 */
void
in_database_save_resources (InDatabase *self,
                            GList      *resources)
{
  g_return_if_fail (IN_IS_DATABASE (self));

  in_adapter_execute (self->adapter, "BEGIN;");

  for (GList *cur = resources; cur; cur = cur->next)
    {
      in_database_save_resource (self, cur->data);
    }

  in_adapter_execute (self->adapter, "COMMIT;");
}

/**
 * in_database_get_issue:
 * @self: a #InDatabase
 * @id: the id of the #InIssue
 *
 * Returns: (transfer full): a #InIssue or NULL if not found
 */
InIssue *
in_database_get_issue (InDatabase  *self,
                       const gchar *id)
{
  return in_issue_data_mapper_get (self->issue_mapper, id);
}

/**
 * in_database_get_note:
 * @self: a #InDatabase
 * @id: the id of the #InNote
 *
 * Returns: (transfer full): a #InNote or NULL if not found
 */
InNote *
in_database_get_note (InDatabase  *self,
                      const gchar *id)
{
  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);

  return in_note_data_mapper_get (self->note_mapper, id);
}

/**
 * in_database_get_all_issues:
 *
 * Returns: (transfer container) (element-type InIssue): all #InIssue in the database
 */
GPtrArray *
in_database_get_all_issues (InDatabase *self)
{
  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);

  return in_issue_data_mapper_get_all (self->issue_mapper);
}

/**
 * in_database_get_notes:
 *
 * Returns: (transfer full): a #GListModel with all #InNote
 */
GPtrArray *
in_database_get_notes (InDatabase *self,
                       InIssue    *issue)
{
  GPtrArray *notes_arr;

  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);

  notes_arr = in_note_data_mapper_get_for_issue (self->note_mapper, IN_PERSONAL_RESOURCE (issue));
  return notes_arr;
}

/**
 * in_database_get_author:
 * @self: a #InDatabase
 * @personal_resource: a #InPersonalResource
 *
 * Returns: (transfer full): a #InAuthor from the database
 */
InAuthor *
in_database_get_author (InDatabase         *self,
                        InPersonalResource *personal_resource)
{
  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);

  return in_author_data_mapper_get (self->author_mapper, in_personal_resource_get_author_id (personal_resource));
}

/**
 * in_database_get_author_by_id:
 * @self: a #InDatabase
 * @id: the id of the #InAuthor
 *
 * Returns: (transfer full): a #InAuthor from the database
 */
InAuthor *
in_database_get_author_by_id (InDatabase  *self,
                              const gchar *id)
{
  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);

  return in_author_data_mapper_get (self->author_mapper, id);
}

void
in_database_save_etag (InDatabase  *self,
                       const gchar *function,
                       const gchar *etag)
{
  sqlite3_stmt *stmt = NULL;

  g_return_if_fail (IN_IS_DATABASE (self));
  g_return_if_fail (function != NULL);
  g_return_if_fail (etag != NULL);

  // INSERT INTO etags (function, etag) VALUES ?, ?;
  stmt = in_adapter_prepare (self->adapter, (gchar *)"REPLACE INTO etags (function, etag) VALUES (?, ?);");
  sqlite3_bind_text (stmt, 1, function, -1, NULL);
  sqlite3_bind_text (stmt, 2, etag, -1, NULL);
  sqlite3_step (stmt);
}

const gchar *
in_database_get_etag (InDatabase  *self,
                      const gchar *function)
{
  sqlite3_stmt *stmt = NULL;
  const guchar *etag = NULL;

  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);
  g_return_val_if_fail (function != NULL, NULL);

  // SELECT etag from etags WHERE function = ?;
  stmt = in_adapter_prepare (self->adapter, (gchar *)"SELECT etag FROM etags WHERE function = ?;");
  sqlite3_bind_text (stmt, 1, function, -1, NULL);
  sqlite3_step (stmt);

  etag = sqlite3_column_text (stmt, 0);
  return (gchar *)etag;
}

InProviderInfos *
in_database_get_provider_infos (InDatabase  *self,
                                const gchar *provider,
                                const gchar *baseurl)
{
  g_return_val_if_fail (IN_IS_DATABASE (self), NULL);
  g_return_val_if_fail (provider != NULL, NULL);
  g_return_val_if_fail (baseurl != NULL, NULL);

  return in_provider_infos_data_mapper_get (self->provider_infos_mapper, provider, baseurl);
}
