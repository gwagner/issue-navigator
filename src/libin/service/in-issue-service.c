/* in-issue-service.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-issue-service.h"
#include "in-issue-provider.h"

struct _InIssueService
{
  GObject parent_instance;

  PeasExtensionSet *extensions;
};

G_DEFINE_TYPE (InIssueService, in_issue_service, G_TYPE_OBJECT)

enum {
  START,
  STOP,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

/**
 * in_issue_service_get_default:
 *
 * Returns: (transfer none): a #InIssueService Singleton
 */
InIssueService *
in_issue_service_get_default (void)
{
  static InIssueService *instance;

  g_assert (!instance || IN_IS_ISSUE_SERVICE (instance));

  if (g_once_init_enter (&instance))
    g_once_init_leave (&instance, g_object_new (IN_TYPE_ISSUE_SERVICE, NULL));

  return instance;
}

static void
in_issue_service_finalize (GObject *object)
{
  InIssueService *self = (InIssueService *)object;

  g_clear_pointer (&self->extensions, g_object_unref);

  G_OBJECT_CLASS (in_issue_service_parent_class)->finalize (object);
}

static void
in_issue_service_class_init (InIssueServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_issue_service_finalize;

  signals [START] =
    g_signal_new ("start",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);

  signals [STOP] =
    g_signal_new ("stop",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  0);
}

/* static void */
/* in_issue_service_extension_load (PeasExtensionSet *set, */
/*                                  PeasPluginInfo   *info, */
/*                                  InIssueProvider  *provider, */
/*                                  gpointer          user_data) */
/* { */
/*   g_debug ("Loading plugin: %s", peas_plugin_info_get_name (info)); */
/*   in_issue_provider_load (provider); */
/* } */

static void
in_issue_service_init (InIssueService *self)
{
  self->extensions = NULL;
  /* self->extensions = peas_extension_set_new (NULL, IN_TYPE_ISSUE_PROVIDER, NULL); */

  /* g_signal_connect (self->extensions, "extension-added", G_CALLBACK (in_issue_service_extension_load), NULL); */

  /* peas_extension_set_foreach (self->extensions, */
  /*                             (PeasExtensionSetForeachFunc) in_issue_service_extension_load, */
  /*                             NULL); */
}

static gboolean
stop_sync (gpointer user_data)
{
  InIssueService *self = IN_ISSUE_SERVICE (user_data);

  g_signal_emit (self, signals[STOP], 0);

  return G_SOURCE_REMOVE;
}

static void
in_issue_service_extension_sync_cb (GObject      *object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  InIssueProvider *provider = (InIssueProvider *)object;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));

  in_issue_provider_fetch_issues_finish (provider, result, &error);
  stop_sync (user_data);
}

static void
in_issue_service_extension_sync (PeasExtensionSet *set,
                                 PeasPluginInfo   *info,
                                 InIssueProvider  *provider,
                                 gpointer          user_data)
{
  in_issue_provider_fetch_issues_async (provider, NULL, in_issue_service_extension_sync_cb, user_data);
}

static gboolean
start_sync (gpointer user_data)
{
  InIssueService *self = IN_ISSUE_SERVICE (user_data);

  g_signal_emit (self, signals[START], 0);

  return G_SOURCE_REMOVE;
}

static gint
in_issue_service_sync (gpointer user_data)
{
  InIssueService *self = IN_ISSUE_SERVICE (user_data);

  if (self->extensions != NULL)
    {
      start_sync (self);
      peas_extension_set_foreach (self->extensions, (PeasExtensionSetForeachFunc) in_issue_service_extension_sync, self);
    }

  return G_SOURCE_CONTINUE;
}

void
in_issue_service_trigger (InIssueService *self)
{
  g_print ("%s\n", "service triggered...");

  g_timeout_add_seconds (20, in_issue_service_sync, self);
}

/**
 * in_issue_service_connect:
 * @self: a #InIssueService
 * @cb: (scope call): a #GCallback
 * @data: user data
 */
void
in_issue_service_connect (InIssueService *self,
                          GCallback       cb,
                          gpointer        data)
{
  PeasEngine *engine = NULL;
  const GList *plugin_list = NULL;

  g_return_if_fail (IN_IS_ISSUE_SERVICE (self));

  engine = peas_engine_get_default ();
  plugin_list = peas_engine_get_plugin_list (engine);

  for (GList *cur = (GList *)plugin_list; cur; cur = cur->next)
    {
      PeasExtension *exten = peas_extension_set_get_extension (self->extensions, cur->data);
      if (exten)
        g_signal_connect_swapped (exten, "updated", cb, data);
    }
}

void
in_issue_service_get_comments (InIssueService      *self,
                               InIssue             *issue,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  PeasEngine *engine = NULL;
  const GList *plugin_list = NULL;

  g_return_if_fail (IN_IS_ISSUE_SERVICE (self));
  g_return_if_fail (IN_IS_ISSUE (issue));

  engine = peas_engine_get_default ();
  plugin_list = peas_engine_get_plugin_list (engine);

  for (GList *cur = (GList *)plugin_list; cur; cur = cur->next)
    {
      PeasExtension *exten = peas_extension_set_get_extension (self->extensions, cur->data);
      if (exten)
        in_issue_provider_fetch_comments_async (IN_ISSUE_PROVIDER (exten), issue, NULL, callback, user_data);
    }

}

void
in_issue_service_set_provider (InIssueService   *self,
                               PeasExtensionSet *set)
{
  self->extensions = set;
}
