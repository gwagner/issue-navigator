/* in-issue-service.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>
#include "in-issue.h"
#include <libpeas/peas.h>

G_BEGIN_DECLS

#define IN_TYPE_ISSUE_SERVICE (in_issue_service_get_type())

G_DECLARE_FINAL_TYPE (InIssueService, in_issue_service, IN, ISSUE_SERVICE, GObject)

InIssueService *in_issue_service_get_default  (void);
void            in_issue_service_set_provider (InIssueService   *self,
                                               PeasExtensionSet *set);
void            in_issue_service_trigger      (InIssueService      *self);
void            in_issue_service_connect      (InIssueService      *self,
                                               GCallback            cb,
                                               gpointer             data);
void            in_issue_service_get_comments (InIssueService      *self,
                                               InIssue             *issue,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data);
G_END_DECLS
