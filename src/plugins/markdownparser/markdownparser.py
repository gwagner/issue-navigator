import gi
import requests
from mistletoe.base_renderer import BaseRenderer
from mistletoe import Document
from pygments.lexers import get_lexer_by_name, guess_lexer
from pygments.formatters import PangoMarkupFormatter
from pygments import highlight
gi.require_version('In', '0.0')
from gi.repository import In, GObject, GLib, Gdk, Gtk

class GitlabPangoParser(GObject.Object, In.PangoParser):
    __gtype_name__ = 'GitlabPangoParser'

    def do_parse(self, text):
        box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        with MarkdownToPangoRenderer() as renderer:
            list = renderer.render(Document(text))
            for item in list:
                if isinstance(item, str):
                    lbl = Gtk.Label.new(None)
                    lbl.set_markup(item)
                    lbl.set_wrap(True)
                    lbl.set_xalign(0.0)
                    lbl.get_style_context().add_class("issue-description")
                    box.append(lbl)
                if isinstance(item, Image):
                    r = requests.get(item.src)
                    b = GLib.Bytes.new(r.content)
                    imagetexture = Gdk.Texture.new_from_bytes(b)
                    height = imagetexture.get_height()
                    image = Gtk.Picture.new_for_paintable(imagetexture)
                    # image.set_hexpand(True)
                    # image.set_property("height-request", height)
                    if imagetexture.get_width() > 1200:
                        image.set_property("width-request", 1200)
                    else:
                        image.set_can_shrink(False)
                    box.append(image)
            return box

        return box

class MarkdownToPangoRenderer(BaseRenderer):
    list = []
    current = ""

    def render_strong(self, token):
        self.append_text("<b>")
        for child in token.children:
            self.render(child)
        self.append_text("</b>")

    def render_emphasis(self, token):
        self.append_text("<i>")
        for child in token.children:
            self.render(child)
        self.append_text("</i>")

    def render_inline_code(self, token):
        self.append_text('<span size="small"><tt>')
        for child in token.children:
            self.render(child)
        self.append_text('</tt></span>')

    def render_raw_text(self, token):
        print(token)
        self.append_text (GLib.markup_escape_text(token.content))

    def render_strikethrough(self, token):
        template = '<s>{}</s>'
        return template.format(self.render_inner(token))

    def render_image(self, token):
        print(token)
        self.append_image(Image(token.src))

    def render_link(self, token):
        self.append_text('<a href="{target}">'.format(target=token.target))
        for child in token.children:
            self.render(child)
        self.append_text('</a>')
        # template = '<a href="{target}"{title}>{inner}</a>'
        # target = GLib.markup_escape_text(token.target)
        # if token.title:
        #     title = ' title="{}"'.format(GLib.markup_escape_text(token.title))
        # else:
        #     title = ''
        # inner = self.render_inner(token)
        # return template.format(target=target, title=title, inner=inner)

    def render_auto_link(self, token):

        self.append_text('<a href="{target}">'.format(target=token.target))
        for child in token.children:
            self.render(child)
        self.append_text('</a>')
        # print(token)
        # template = '<a href="{target}">{inner}</a>'
        # if token.mailto:
        #     target = 'mailto:{}'.format(token.target)
        # else:
        #     target = self.escape_url(token.target)
        # inner = self.render_inner(token)
        # return template.format(target=target, inner=inner)

    def render_escape_sequence(self, token):
        return self.render_inner(token)

    def render_heading(self, token):
        level = {
            1: 'xx-large',
            2: 'x-large',
            3: 'large',
            4: 'medium',
            5: 'small',
            6: 'x-small'
        }
        self.append_text('<span size="{level}">'.format(level=level.get(token.level)))
        for child in token.children:
            self.render(child)
        self.append_text('</span>\n')

    def render_quote(self, token):
        self.append_text('\n\t<span color="grey">')
        for child in token.children:
            self.render(child)
        self.append_text('</span>\n')

    def render_paragraph(self, token):
        print(token)
        for child in token.children:
            self.render(child)
        self.append_text("\n")

    def render_block_code(self, token):

        self.append_text('<span size="small">')
        for child in token.children:
            self.render(child)
        self.append_text('</span>')
        # code = self.render_inner(token)
        # lexer = guess_lexer(code)
        # formatter = PangoMarkupFormatter()
        # return '<span size="small">' + highlight(code, lexer, formatter) + '</span>'

    def render_list(self, token):
        for child in token.children:
            self.render(child)

    def render_list_item(self, token):
        self.append_text('• ')
        for child in token.children:
            self.render(child)

    def render_table(self, token):
        # TODO
        return self.render_inner(token)

    def render_table_row(self, token):
        # TODO
        return self.render_inner(token)

    def render_table_cell(self, token):
        # TODO
        return self.render_inner(token)

    def render_thematic_break(self, token):
        # TODO
        return '\n---\n\n'

    def render_line_break(self, token):
        self.append_text("\n")

    def render_document(self, token):
        self.list.clear()
        self.current = ""
        # self.footnotes.update(token.footnotes)
        # self.visit_node(token)

        for child in token.children:
            self.render(child)

        if self.current:
            self.list.append(self.current)

        print(len(self.list))

        return self.list


        #inner = '\n'.join([])
        # return '{}'.format(inner).strip() if inner else ''

    def visit_node(self, token):
        print(token)
        if hasattr(token, 'children'):
            for t in token.children:
                self.visit_node(t)

    def append_text(self, text):
        self.current += text

    def append_image(self, image):
        self.list.append(self.current)
        self.current = ""
        self.list.append(image)

class Image:

    def __init__(self, src):
        self.src = "https://gitlab.gnome.org/GNOME/gnome-builder/" + src
