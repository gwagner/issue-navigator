#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define IN_TYPE_GITLAB_ISSUE_PROVIDER (in_gitlab_issue_provider_get_type())

G_DECLARE_FINAL_TYPE (InGitlabIssueProvider, in_gitlab_issue_provider, IN, GITLAB_ISSUE_PROVIDER, GObject)

InGitlabIssueProvider *in_gitlab_issue_provider_new (void);

G_END_DECLS
