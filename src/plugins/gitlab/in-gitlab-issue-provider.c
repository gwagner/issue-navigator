/* in-gitlab-issue-provider.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-application.h"
#include "in-core.h"
#include "in-gitlab-issue-provider.h"
#include "in-gitlab-proxy-call.h"
#include "in-gitlab-proxy.h"
#include "in-issue-provider.h"
#include <json-glib/json-glib.h>
#include <libsoup/soup.h>
#include <rest/rest-proxy.h>

struct _InGitlabIssueProvider
{
  GObject parent_instance;
  InProviderInfos *infos;

  RestProxy *proxy;
  gboolean logged_in;
};

static void in_issue_provider_iface (InIssueProviderInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (InGitlabIssueProvider, in_gitlab_issue_provider, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (IN_TYPE_ISSUE_PROVIDER, in_issue_provider_iface))

InGitlabIssueProvider *
in_gitlab_issue_provider_new (void)
{
  return g_object_new (IN_TYPE_GITLAB_ISSUE_PROVIDER, NULL);
}

static void
in_gitlab_issue_provider_finalize (GObject *object)
{
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) object;

  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (in_gitlab_issue_provider_parent_class)->finalize (object);
}

static void
in_gitlab_issue_provider_class_init (InGitlabIssueProviderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = in_gitlab_issue_provider_finalize;
}

static void
in_gitlab_issue_provider_init (InGitlabIssueProvider *self)
{
  self->logged_in = FALSE;
}

static void
in_gitlab_issue_provider_load (InIssueProvider *provider)
{
  InGitlabIssueProvider *self = IN_GITLAB_ISSUE_PROVIDER (provider);
  SoupLogger *logger = soup_logger_new (SOUP_LOGGER_LOG_HEADERS);

  InApplication *app = IN_APPLICATION (g_application_get_default ());
  InDatabase *db = in_application_get_database (app);
  const gchar *baseurl = "https://gitlab.gnome.org/api/v4";
  InGitlabProxy *gitlab_proxy = in_gitlab_proxy_new ("https://gitlab.gnome.org/oauth/authorize",
                                                     "https://gitlab.gnome.org/oauth/token",
                                                     "http://example.com",
                                                     "0db71449831fa59aa5aaff188f3cc78b8e8e530d22caca5d5f7f84069f8b6fa4",
                                                     "d70b4f400a680af3d37a0ed1d9c3ea17b74cb9b7bf1f2e8bace60f21e521acc6",
                                                     baseurl,
                                                     db);
  InProviderInfos *infos = in_database_get_provider_infos (db, "Gitlab", baseurl);
  if (!infos)
    {
      infos = in_provider_infos_new ();
      in_provider_infos_set_name (infos, "Gitlab");
      in_provider_infos_set_url (infos, baseurl);
      in_provider_infos_set_client_id (infos, rest_oauth2_proxy_get_client_id (REST_OAUTH2_PROXY (gitlab_proxy)));
      in_provider_infos_set_client_secret (infos, rest_oauth2_proxy_get_client_secret (REST_OAUTH2_PROXY (gitlab_proxy)));
      in_database_save_resource (db, G_OBJECT (infos));
    }
  else
    {
      rest_oauth2_proxy_set_access_token (REST_OAUTH2_PROXY (gitlab_proxy), in_provider_infos_get_access_token (infos));
      rest_oauth2_proxy_set_refresh_token (REST_OAUTH2_PROXY (gitlab_proxy), in_provider_infos_get_refresh_token (infos));
      rest_oauth2_proxy_set_expiration_date (REST_OAUTH2_PROXY (gitlab_proxy), in_provider_infos_get_expiration_date (infos));
      self->logged_in = TRUE;
    }
  self->infos = infos;
  self->proxy = REST_PROXY (gitlab_proxy);
  rest_proxy_add_soup_feature (self->proxy, (SoupSessionFeature *) logger);

  SoupCache *cache = soup_cache_new (NULL, SOUP_CACHE_SINGLE_USER);
  rest_proxy_add_soup_feature (self->proxy, (SoupSessionFeature *) cache);
}

static gboolean
in_gitlab_issue_provider_login_required (InIssueProvider *provider)
{
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) provider;

  g_return_val_if_fail (IN_IS_GITLAB_ISSUE_PROVIDER (self), FALSE);

  // TODO: only necessary when no access_token is available
  if (in_provider_infos_get_access_token (self->infos) == NULL)
    {
      return TRUE;
    }
  return FALSE;
}

typedef struct
{
  InGitlabIssueProvider *self;
  GtkEntry *entry;
  gchar *code_verifier;
  GtkDialog *dialog;
} ResponseData;

static void
fetch_auth_token_done (GObject      *object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  g_autoptr(GError) error = NULL;
  ResponseData *data = (ResponseData *) user_data;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));

  InApplication *app = IN_APPLICATION (g_application_get_default ());
  InDatabase *db = in_application_get_database (app);

  rest_oauth2_proxy_fetch_access_token_finish (REST_OAUTH2_PROXY (object), result, &error);
  in_provider_infos_set_access_token (data->self->infos, rest_oauth2_proxy_get_access_token (REST_OAUTH2_PROXY (object)));
  in_provider_infos_set_refresh_token (data->self->infos, rest_oauth2_proxy_get_refresh_token (REST_OAUTH2_PROXY (object)));
  in_provider_infos_set_expiration_date (data->self->infos, rest_oauth2_proxy_get_expiration_date (REST_OAUTH2_PROXY (object)));
  in_database_save_resource (db, G_OBJECT (data->self->infos));
  data->self->logged_in = TRUE;
  gtk_window_destroy (GTK_WINDOW (data->dialog));
}

static void
fetch_auth_token (GtkDialog *dialog,
                  gint response_id,
                  gpointer user_data)
{
  ResponseData *data = (ResponseData *) user_data;
  data->dialog = dialog;

  if (response_id == GTK_RESPONSE_OK)
    {
      const gchar *auth_token = gtk_editable_get_text (GTK_EDITABLE (data->entry));
      rest_oauth2_proxy_fetch_access_token_async (REST_OAUTH2_PROXY (data->self->proxy), auth_token, data->code_verifier, NULL, fetch_auth_token_done, data);
    }
}

static void
in_gitlab_issue_provider_login (InIssueProvider *provider,
                                GtkDialog *dialog)
{
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) provider;
  g_autoptr (GString) url_text = NULL;
  g_autoptr (RestPkceCodeChallenge) pkce = NULL;
  GtkWidget *content_area;
  GtkWidget *url;
  GtkWidget *entry;
  gchar *state;

  g_return_if_fail (IN_IS_GITLAB_ISSUE_PROVIDER (self));

  gtk_window_set_title (GTK_WINDOW (dialog), "Gitlab");

  pkce = rest_pkce_code_challenge_new_random ();

  content_area = gtk_dialog_get_content_area (dialog);
  url_text = g_string_new (NULL);
  g_string_append_printf (url_text, "<a href=\"%s\">Authorize App</a>",
                          rest_oauth2_proxy_build_authorization_url (REST_OAUTH2_PROXY (self->proxy),
                                                                     rest_pkce_code_challenge_get_challenge (pkce),
                                                                     NULL,
                                                                     &state));

  g_string_replace (url_text, "&", "&amp;", -1);
  url = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (url), url_text->str);
  gtk_label_set_wrap (GTK_LABEL (url), TRUE);
  gtk_label_set_wrap_mode (GTK_LABEL (url), PANGO_WRAP_CHAR);
  gtk_label_set_width_chars (GTK_LABEL (url), 30);
  gtk_label_set_max_width_chars (GTK_LABEL (url), 30);
  gtk_box_append (GTK_BOX (content_area), url);

  entry = gtk_entry_new ();
  gtk_box_append (GTK_BOX (content_area), entry);

  ResponseData *data = g_new (ResponseData, 1);
  data->entry = GTK_ENTRY (entry);
  data->self = self;
  data->code_verifier = g_strdup (rest_pkce_code_challenge_get_verifier (pkce));

  g_signal_connect (dialog, "response", G_CALLBACK (fetch_auth_token), data);
}

static void
parse_issue (JsonNode *issue_node,
             InDatabase *db,
             GList **issues_arr,
             GList **author_arr)
{
  InIssue *issue = NULL;
  g_autoptr (InIssue) db_issue = NULL;

  JsonObject *jobj = json_node_get_object (issue_node);
  JsonNode *jauthor = json_object_get_member (jobj, "author");
  InAuthor *author = IN_AUTHOR (json_gobject_deserialize (IN_TYPE_AUTHOR, jauthor));

  g_autoptr (InAuthor) db_author = in_database_get_author_by_id (db, in_author_get_id (author));
  if (db_author == NULL)
    {
      *author_arr = g_list_append (*author_arr, author);
    }

  issue = IN_ISSUE (json_gobject_deserialize (IN_TYPE_ISSUE, issue_node));
  in_personal_resource_set_author_id (IN_PERSONAL_RESOURCE (issue), in_author_get_id (author));
  db_issue = in_database_get_issue (db, in_personal_resource_get_id (IN_PERSONAL_RESOURCE (issue)));
  if (db_issue == NULL)
    *issues_arr = g_list_append (*issues_arr, issue);
  else
    g_object_unref (issue);

  if (db_author != NULL)
    g_object_unref (author);
}

static void
_parse_issues_cb (GObject *object,
                  GAsyncResult *result,
                  gpointer user_data)
{
  RestProxyCall *call = (RestProxyCall *) object;
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) g_task_get_source_object (task);
  g_autoptr (JsonParser) parser = NULL;
  JsonNode *root;
  JsonArray *issues;
  GList *arr = NULL, *author_arr = NULL;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  InApplication *app = IN_APPLICATION (g_application_get_default ());
  InDatabase *db = in_application_get_database (app);

  rest_proxy_call_invoke_finish (call, result, &error);
  if (error)
    {
      if (rest_proxy_call_get_status_code (call) == SOUP_STATUS_UNAUTHORIZED)
        {
          self->logged_in = FALSE;
          in_provider_infos_set_access_token (self->infos, NULL);
          in_issue_provider_emit_relogin_required (IN_ISSUE_PROVIDER (self));
        }
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  parser = json_parser_new ();
  json_parser_load_from_data (parser,
                              rest_proxy_call_get_payload (call),
                              rest_proxy_call_get_payload_length (call),
                              &error);
  root = json_parser_get_root (parser);

  issues = json_node_get_array (root);
  for (guint i = 0; i < json_array_get_length (issues); i++)
    {
      JsonNode *n = json_array_get_element (issues, i);
      parse_issue (n, db, &arr, &author_arr);
    }

  in_database_save_resources (db, author_arr);
  in_database_save_resources (db, arr);
  in_issue_provider_emit_updated (IN_ISSUE_PROVIDER (self));

  g_clear_list (&arr, g_object_unref);
  g_clear_list (&author_arr, g_object_unref);

  // the last one has to finish the task
  gint i = GPOINTER_TO_INT (g_task_get_task_data (task)) - 1;
  g_task_set_task_data (task, GINT_TO_POINTER (i), NULL);
  if (i == 0)
    g_task_return_boolean (task, TRUE);
}

static void
in_gitlab_issue_provider_fetch_issues_cb (GObject *object,
                                          GAsyncResult *result,
                                          gpointer user_data)
{
  RestProxyCall *call = (RestProxyCall *) object;
  g_autoptr (GTask) task = user_data;
  InGitlabIssueProvider *self;

  self = (InGitlabIssueProvider *) g_task_get_source_object (task);
  _parse_issues_cb (object, result, g_object_ref (user_data));
  if (g_task_had_error (task))
    return;

  const gchar *pages_str = rest_proxy_call_lookup_response_header (call, "x-total-pages");
  if (pages_str == NULL)
    {
      g_task_return_boolean (task, FALSE);
      return;
    }
  gint pages = atoi (pages_str);

  g_task_set_task_data (task, GINT_TO_POINTER (pages-2), NULL);
  for (int i = 2; i < pages; i++)
    {
      g_autoptr (RestProxyCall) fcall = rest_proxy_new_call (self->proxy);

      rest_proxy_call_set_method (fcall, "GET");
      rest_proxy_call_set_function (fcall, rest_proxy_call_get_function (call));
      rest_proxy_call_add_param (fcall, "per_page", "100");
      rest_proxy_call_add_param (fcall, "page", g_strdup_printf ("%d", i));
      rest_proxy_call_invoke_async (fcall, NULL, _parse_issues_cb, g_object_ref (task));
    }
}

/**
 * in_gitlab_issue_provider_fetch_issues_async:
 * @self: an #InGitlabIssueProvider
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 */
void
in_gitlab_issue_provider_fetch_issues_async (InIssueProvider *provider,
                                             GCancellable *cancellable,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data)
{
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) provider;
  g_autoptr (GTask) task = NULL;
  g_autoptr (RestProxyCall) call = NULL;
  g_autoptr(GDateTime) now = NULL;
  GDateTime *expiration_date = NULL;

  g_return_if_fail (IN_IS_GITLAB_ISSUE_PROVIDER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  InApplication *app = IN_APPLICATION (g_application_get_default ());
  InDatabase *db = in_application_get_database (app);

  if (!self->logged_in)
    return;

  now = g_date_time_new_now_local ();
  expiration_date = rest_oauth2_proxy_get_expiration_date (REST_OAUTH2_PROXY (self->proxy));

  // access token expired -> refresh
  if (g_date_time_compare (now, expiration_date) > 0)
    {
      rest_oauth2_proxy_refresh_access_token (REST_OAUTH2_PROXY (self->proxy));
      in_provider_infos_set_access_token (self->infos, rest_oauth2_proxy_get_access_token (REST_OAUTH2_PROXY (self->proxy)));
      in_provider_infos_set_refresh_token (self->infos, rest_oauth2_proxy_get_refresh_token (REST_OAUTH2_PROXY (self->proxy)));

      in_database_save_resource (db, G_OBJECT (self->infos));
    }

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, in_gitlab_issue_provider_fetch_issues_async);

  call = rest_proxy_new_call (self->proxy);

  rest_proxy_call_set_method (call, "GET");
  rest_proxy_call_set_function (call, "/projects/426/issues");
  rest_proxy_call_add_param (call, "per_page", "100");
  rest_proxy_call_invoke_async (call, cancellable, in_gitlab_issue_provider_fetch_issues_cb, g_steal_pointer (&task));
}

/**
 * in_gitlab_issue_provider_fetch_issues_finish:
 * @self: an #InGitlabIssueProvider
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns:
 */
gboolean
in_gitlab_issue_provider_fetch_issues_finish (InGitlabIssueProvider  *self,
                                              GAsyncResult           *result,
                                              GError                **error)
{
  g_return_val_if_fail (IN_IS_GITLAB_ISSUE_PROVIDER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
in_gitlab_issue_provider_fetch_comments_cb (GObject      *object,
                                            GAsyncResult *result,
                                            gpointer      user_data)
{
  RestProxyCall *call = (RestProxyCall *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr (JsonParser) parser = NULL;
  JsonNode *root = NULL;
  JsonArray *notes = NULL;
  GList *author_arr = NULL, *notes_arr = NULL;

  g_assert (G_IS_OBJECT (object));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  InApplication *app = IN_APPLICATION (g_application_get_default ());
  InDatabase *db = in_application_get_database (app);
  InIssue *parent = g_task_get_task_data (task);

  if (rest_proxy_call_get_status_code (call) == REST_PROXY_ERROR_HTTP_NOT_MODIFIED)
    {
      g_task_return_pointer (task, in_database_get_notes (db, IN_ISSUE (parent)), (GDestroyNotify) g_ptr_array_unref);
      return;
    }

  parser = json_parser_new ();
  json_parser_load_from_data (parser,
                              rest_proxy_call_get_payload (call),
                              rest_proxy_call_get_payload_length (call),
                              &error);
  if (error != NULL)
    g_task_return_error (task, error);
  root = json_parser_get_root (parser);
  notes = json_node_get_array (root);

  for (guint i = 0; i < json_array_get_length (notes); i++)
    {
      InNote *note;
      JsonNode *note_node = json_array_get_element (notes, i);
      InNote *db_note = NULL;

      JsonObject *jobj = json_node_get_object (note_node);
      JsonNode *jauthor = json_object_get_member (jobj, "author");
      InAuthor *author = IN_AUTHOR (json_gobject_deserialize (IN_TYPE_AUTHOR, jauthor));
      author_arr = g_list_append (author_arr, author);

      note = IN_NOTE (json_gobject_deserialize (IN_TYPE_NOTE, note_node));
      in_note_set_parent_id (note, atoi (in_personal_resource_get_id (parent)));
      in_personal_resource_set_author_id (IN_PERSONAL_RESOURCE (note), in_author_get_id (author));

      db_note = in_database_get_note (db, in_personal_resource_get_id (IN_PERSONAL_RESOURCE (note)));
      if (db_note == NULL)
        notes_arr = g_list_append (notes_arr, note);
      // TODO: else update!
    }

  in_database_save_resources (db, author_arr);
  in_database_save_resources (db, notes_arr);

  // TODO: directly gather all objects in a GPtrArray
  g_task_return_pointer (task, in_database_get_notes (db, IN_ISSUE (parent)), (GDestroyNotify) g_ptr_array_unref);
}

/**
 * in_gitlab_issue_provider_fetch_comments_async:
 * @self: an #InGitlabIssueProvider
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 */
void
in_gitlab_issue_provider_fetch_comments_async (InIssueProvider *provider,
                                               InIssue *issue,
                                               GCancellable *cancellable,
                                               GAsyncReadyCallback callback,
                                               gpointer user_data)
{
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) provider;
  g_autoptr (GTask) task = NULL;
  g_autoptr (RestProxyCall) call = NULL;
  g_autoptr (GString) s = NULL;

  g_assert (IN_IS_GITLAB_ISSUE_PROVIDER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, in_gitlab_issue_provider_fetch_comments_async);

  g_task_set_task_data (task, g_object_ref (issue), g_object_unref);
  /* g_task_run_in_thread (task, in_gitlab_issue_provider_fetch_note_cb); */

  s = g_string_new ("projects/426/issues/");
  g_string_append (s, in_personal_resource_get_id (IN_PERSONAL_RESOURCE (issue)));
  g_string_append (s, "/notes");

  call = rest_proxy_new_call (self->proxy);
  rest_proxy_call_set_method (call, "GET");
  rest_proxy_call_set_function (call, s->str);
  rest_proxy_call_add_param (call, "sort", "asc");
  rest_proxy_call_invoke_async (call, cancellable, in_gitlab_issue_provider_fetch_comments_cb, g_steal_pointer (&task));
}

/**
 * in_gitlab_issue_provider_fetch_comments_finish:
 * @self: an #InGitlabIssueProvider
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns:
 */
gboolean
in_gitlab_issue_provider_fetch_comments_finish (InIssueProvider *provider,
                                                GAsyncResult *result,
                                                GPtrArray **comments,
                                                GError **error)
{
  InGitlabIssueProvider *self = (InGitlabIssueProvider *) provider;
  g_autoptr (GPtrArray) arr = NULL;
  gboolean ret = FALSE;

  g_assert (IN_IS_GITLAB_ISSUE_PROVIDER (self));
  g_assert (g_task_is_valid (result, self));

  arr = g_task_propagate_pointer (G_TASK (result), error);
  ret = (arr != NULL);

  if (comments != NULL)
    *comments = g_steal_pointer (&arr);

  return ret;
}

static void
in_issue_provider_iface (InIssueProviderInterface *iface)
{
  iface->load = in_gitlab_issue_provider_load;
  iface->login_required = in_gitlab_issue_provider_login_required;
  iface->login = in_gitlab_issue_provider_login;
  /* iface->fetch_issues = in_gitlab_issue_provider_fetch_issues; */
  iface->fetch_issues_async = in_gitlab_issue_provider_fetch_issues_async;
  iface->fetch_issues_finish = in_gitlab_issue_provider_fetch_issues_finish;
  iface->fetch_comments_async = in_gitlab_issue_provider_fetch_comments_async;
  iface->fetch_comments_finish = in_gitlab_issue_provider_fetch_comments_finish;
}

