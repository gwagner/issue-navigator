/* in-gitlab-proxy.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-gitlab-proxy.h"
#include "in-gitlab-proxy-call.h"

struct _InGitlabProxy
{
  RestOAuth2Proxy parent_instance;

  InDatabase *database;
};

G_DEFINE_FINAL_TYPE (InGitlabProxy, in_gitlab_proxy, REST_TYPE_OAUTH2_PROXY)

enum {
  PROP_0,
  PROP_DATABASE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

InGitlabProxy *
in_gitlab_proxy_new (const gchar *authurl,
                     const gchar *tokenurl,
                     const gchar *redirecturl,
                     const gchar *client_id,
                     const gchar *client_secret,
                     const gchar *url,
                     InDatabase  *db)
{
  return g_object_new (IN_TYPE_GITLAB_PROXY,
                       "url-format", url,
                       "database", db,
                       "auth-url", authurl,
                       "token-url", tokenurl,
                       "redirect-uri", redirecturl,
                       "client-id", client_id,
                       "client-secret", client_secret,
                       "binding-required", FALSE,
                       NULL);
}

static void
in_gitlab_proxy_finalize (GObject *object)
{
  InGitlabProxy *self = (InGitlabProxy *)object;

  g_clear_object (&self->database);

  G_OBJECT_CLASS (in_gitlab_proxy_parent_class)->finalize (object);
}

static void
in_gitlab_proxy_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  InGitlabProxy *self = IN_GITLAB_PROXY (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_gitlab_proxy_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  InGitlabProxy *self = IN_GITLAB_PROXY (object);

  switch (prop_id)
    {
    case PROP_DATABASE:
      self->database = g_value_dup_object (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

RestProxyCall *
in_gitlab_proxy_new_call (RestProxy *proxy)
{
  InGitlabProxy *self = IN_GITLAB_PROXY (proxy);
  return REST_PROXY_CALL (in_gitlab_proxy_call_new (self, self->database));
}

static void
in_gitlab_proxy_class_init (InGitlabProxyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RestProxyClass *rest_class = REST_PROXY_CLASS (klass);

  object_class->finalize = in_gitlab_proxy_finalize;
  object_class->get_property = in_gitlab_proxy_get_property;
  object_class->set_property = in_gitlab_proxy_set_property;
  /* rest_class->new_call = in_gitlab_proxy_new_call; */

  properties [PROP_DATABASE] =
    g_param_spec_object ("database",
                         "Database",
                         "Database",
                         IN_TYPE_DATABASE,
                         (G_PARAM_WRITABLE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_DATABASE,
                                   properties [PROP_DATABASE]);
}

static void
in_gitlab_proxy_init (InGitlabProxy *self)
{
}
