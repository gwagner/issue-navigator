/* in-gitlab-proxy-call.c
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "in-gitlab-proxy-call.h"

struct _InGitlabProxyCall
{
  RestProxyCall parent_instance;
  InDatabase *database;
};

G_DEFINE_FINAL_TYPE (InGitlabProxyCall, in_gitlab_proxy_call, REST_TYPE_PROXY_CALL)

enum {
  PROP_0,
  PROP_DATABASE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

InGitlabProxyCall *
in_gitlab_proxy_call_new (InGitlabProxy *proxy, InDatabase *db)
{
  return g_object_new (IN_TYPE_GITLAB_PROXY_CALL,
                       "proxy", proxy,
                       "database", db,
                       NULL);
}

static void
in_gitlab_proxy_call_finalize (GObject *object)
{
  InGitlabProxyCall *self = (InGitlabProxyCall *)object;

  g_clear_object (&self->database);

  G_OBJECT_CLASS (in_gitlab_proxy_call_parent_class)->finalize (object);
}

static void
in_gitlab_proxy_call_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  InGitlabProxyCall *self = IN_GITLAB_PROXY_CALL (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
in_gitlab_proxy_call_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  InGitlabProxyCall *self = IN_GITLAB_PROXY_CALL (object);

  switch (prop_id)
    {
    case PROP_DATABASE:
      self->database = g_value_dup_object (value);
      g_object_notify_by_pspec (object, pspec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static gboolean
in_gitlab_proxy_call_prepare (RestProxyCall  *call,
                              GError        **error)
{
  g_autofree gchar *f = NULL;
  const gchar *function = NULL;
  const gchar *etag = NULL;
  RestParam *page;
  InGitlabProxyCall *self = IN_GITLAB_PROXY_CALL (call);

  g_return_val_if_fail (IN_IS_GITLAB_PROXY_CALL (call), FALSE);

  function = rest_proxy_call_get_function (call);
  page = rest_proxy_call_lookup_param (call, "page");
  if (page)
    {
      const gchar *p = rest_param_get_content (page);
      f = g_strdup_printf ("%s?page=%s", function, p);
    }
  else
    f = g_strdup (function);

  etag = in_database_get_etag (self->database, f);
  if (etag != NULL)
    rest_proxy_call_add_header (call, "If-None-Match", etag);

  return TRUE;
}

static void
in_gitlab_proxy_call_finish (RestProxyCall *call)
{
  g_autofree gchar *f = NULL;
  const gchar *function = NULL;
  const gchar *etag = NULL;
  RestParam *page;
  gint status;
  InGitlabProxyCall *self = IN_GITLAB_PROXY_CALL (call);

  g_return_if_fail (IN_IS_GITLAB_PROXY_CALL (self));

  status = rest_proxy_call_get_status_code (call);

  if (status >= 200 && status < 300)
    {
      function = rest_proxy_call_get_function (call);

      page = rest_proxy_call_lookup_param (call, "page");
      if (page)
        {
          const gchar *p = rest_param_get_content (page);
          f = g_strdup_printf ("%s?page=%s", function, p);
        }
      else
        f = g_strdup (function);

      etag = rest_proxy_call_lookup_response_header (call, "ETag");
      in_database_save_etag (self->database, f, etag);
    }
}

static void
in_gitlab_proxy_call_class_init (InGitlabProxyCallClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RestProxyCallClass *call_class = REST_PROXY_CALL_CLASS (klass);

  object_class->finalize = in_gitlab_proxy_call_finalize;
  object_class->get_property = in_gitlab_proxy_call_get_property;
  object_class->set_property = in_gitlab_proxy_call_set_property;
  /* call_class->prepare = in_gitlab_proxy_call_prepare; */
  /* call_class->finish = in_gitlab_proxy_call_finish; */

  properties [PROP_DATABASE] =
    g_param_spec_object ("database",
                         "Database",
                         "Database",
                         IN_TYPE_DATABASE,
                         (G_PARAM_WRITABLE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_DATABASE,
                                   properties [PROP_DATABASE]);
}

static void
in_gitlab_proxy_call_init (InGitlabProxyCall *self)
{
}
