/* in-gitlab-proxy.h
 *
 * Copyright 2021 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <rest/rest.h>
#include "in-database.h"

G_BEGIN_DECLS

#define IN_TYPE_GITLAB_PROXY (in_gitlab_proxy_get_type())

G_DECLARE_FINAL_TYPE (InGitlabProxy, in_gitlab_proxy, IN, GITLAB_PROXY, RestOAuth2Proxy)

InGitlabProxy *in_gitlab_proxy_new (const gchar *authurl,
                                    const gchar *tokenurl,
                                    const gchar *redirecturl,
                                    const gchar *client_id,
                                    const gchar *client_secret,
                                    const gchar *url,
                                    InDatabase  *db);

G_END_DECLS
