#include <glib.h>
#include "in-persistence.h"

static void
test_issue_data_mapper (void)
{
  g_autoptr(InAdapter) adapter = NULL;
  g_autoptr(InIssueDataMapper) mapper = NULL;
  g_autoptr(InIssue) issue = NULL;
  InIssue *db_issue;

  adapter = in_adapter_new ();
  in_adapter_open (adapter, ":memory:");
  mapper = in_issue_data_mapper_new (adapter);

  in_adapter_execute (adapter, "CREATE TABLE IF NOT EXISTS issues ("
        "  id INTEGER PRIMARY KEY,"
        "  author_id INTEGER,"
        "  title TEXT,"
        "  description TEXT,"
        "  state TEXT,"
        "  web_url TEXT,"
        "  created_at TEXT"
        ")");

  issue = in_issue_new ();
  in_issue_set_title (issue, "title");
  in_issue_set_description (issue, "description");
  in_issue_set_state (issue, "state");
  in_issue_set_web_url (issue, "web_url");
  in_issue_set_created_at (issue, g_date_time_new_from_iso8601 ("2020-10-10T10:10:10.010Z", NULL));
  in_personal_resource_set_id (IN_PERSONAL_RESOURCE (issue), "42");

  in_issue_data_mapper_insert (mapper, issue);

  db_issue = in_issue_data_mapper_get (mapper, "42");

  g_assert_cmpstr (in_issue_get_title (issue), ==, in_issue_get_title (db_issue));
  g_assert_cmpstr (in_personal_resource_get_id (IN_PERSONAL_RESOURCE (issue)), ==, in_personal_resource_get_id (IN_PERSONAL_RESOURCE (db_issue)));
  g_assert_cmpstr (in_issue_get_description (issue), ==, in_issue_get_description (db_issue));
  g_assert_cmpstr (in_issue_get_state (issue), ==, in_issue_get_state (db_issue));
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func("/in/data-mapper", test_issue_data_mapper);

  return g_test_run ();
}
